
#import "BaseVC.h"

@interface TermsVC : BaseVC <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webViewTerms;
- (IBAction)backBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;

@end
