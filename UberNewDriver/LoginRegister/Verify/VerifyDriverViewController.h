
#import "BaseVC.h"



#import <UIKit/UIKit.h>
#import "VerifyPageContentViewController.h"

@interface VerifyDriverViewController : BaseVC <UITextFieldDelegate,UIAlertViewDelegate,UIPageViewControllerDataSource>
{
    
}

//- (IBAction)startWalkthrough:(id)sender;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) NSArray *pageTitles2;
@property (strong, nonatomic) NSArray *pageImages2;
@property (strong, nonatomic) NSArray *pageTitles3;

@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundRect;
@property (strong, nonatomic) IBOutlet UIButton *btn_Continue;
@property (strong, nonatomic) IBOutlet UIWebView *webViewVerify;
@property (weak, nonatomic) IBOutlet UIButton *btnTerm;

- (IBAction)ContinueBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)openSwiftbackVerify:(id)sender;

@end