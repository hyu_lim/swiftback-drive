
#import "OTPVC.h"

@interface OTPVC ()

@end

@implementation OTPVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    defaults = [NSUserDefaults standardUserDefaults];
    [lblMobileNo setText:[defaults objectForKey:PREF_USER_PHONE]];
     [txtOTP setDelegate:self];
    
    secondsLeft = 300.0;
    
    if ([defaults boolForKey:@"close"])
    {
        NSDate *closeDate = [defaults objectForKey:@"closeDate"];
        NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:closeDate];
        
        secondsLeft = secondsLeft - secondsBetween;
        [defaults setBool:NO forKey:@"close"];
        [defaults synchronize];
    }

    
    timerForOTPExpiry = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    
    
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    
    
    // Do any additional setup after loading the view.

    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;

}

-(void)appWillResignActive:(NSNotification*)note
{
    
}

-(void)appWillTerminate:(NSNotification*)note
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
    [defaults setBool:YES forKey:@"close"];
    [defaults synchronize];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateTimer
{
    if (secondsLeft > 0)
    {
        secondsLeft -- ;
        minutes = (secondsLeft % 3600) / 60;
        seconds = (secondsLeft %3600) % 60;
        self.lblTimer.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
        NSLog(@"%d %d",minutes,seconds);
        
    }
    else
    {
        [timerForOTPExpiry invalidate];
        timerForOTPExpiry = nil;
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
     
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtOTP resignFirstResponder];
}

- (IBAction)onClickResend:(id)sender {
    if ([APPDELEGATE connected])
    {
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[defaults objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[defaults objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[defaults objectForKey:PARAM_EMAIL] forKey:PARAM_EMAIL];
        
       // NSString *url = [NSString stringWithFormat:@"swift_back/public/%@",FILE_RESEND_OTP];

        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_RESEND_OTP withParamData:dictParam withBlock:^(id response, NSError *error) {
            if (response)
            {
                if ([[response valueForKey:@"success"]boolValue])
                {
                    [APPDELEGATE showToastMessage:NSLocalizedString(@"OTP_SEND_SUCCESSFULLY", nil)];
                }
            }
        }];
    }
}

- (IBAction)onClickSubmit:(id)sender {
    
    if ([APPDELEGATE connected])
        
    {
        
        if ([[[UtilityClass sharedObject] trimString:txtOTP.text] length] < 1)
            
        {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_ENTER_OTP", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            
            [alert show];
            
        }
        
        else
            
        {
            
            
            
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT_", nil)];
            
            NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
            
            [dictParam setObject:txtOTP.text forKey:PARAM_OTP_CODE];
            
            [dictParam setObject:[defaults objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            
            [dictParam setObject:[defaults objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            
            
            
            //NSString *url = [NSString stringWithFormat:@"swift_back/public/provider/%@",FILE_VERIFY_USER];
            
            
            
            AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            
            [helper getDataFromPath:FILE_VERIFY_USER withParamData:dictParam withBlock:^(id response, NSError *error) {
                
                [APPDELEGATE hideLoadingView];
                
                if (response)
                    
                {
                    
                    NSLog(@"Verify %@",response);
                    
                    if ([[response valueForKey:@"success"]boolValue])
                        
                    {
                        
                        [defaults setObject:@"1" forKey:PARAM_OTP_VERIFY];
                        
                        [defaults setObject:[response valueForKey:@"token"] forKey:PREF_USER_TOKEN];
                        [defaults setObject:[response valueForKey:@"id"] forKey:PREF_USER_ID];
                        
                        [defaults setObject:[response objectForKey:PREF_EMAIL] forKey:PREF_EMAIL];
                        [defaults setObject:[response objectForKey:PARAM_LOGIN_BY] forKey:PREF_LOGIN_BY];
                        if (![[response objectForKey:PARAM_LOGIN_BY]isEqualToString:@"manual"]) {
                            [defaults setObject:[response objectForKey:PARAM_SOCIAL_ID] forKey:PREF_SOCIAL_ID];
                        }
                        [defaults setBool:YES forKey:PREF_IS_LOGIN];
                        
                        [defaults setObject:[response valueForKey:@"is_approved"] forKey:PREF_IS_APPROVED];
                        
                        [defaults synchronize];
                        
                        
                        
                        [self performSegueWithIdentifier:SEGUE_TO_VERIFY_DRIVER sender:nil];
                        
                    }
                    else
                    {
                        [APPDELEGATE showToastMessage:[response objectForKey:@"error"]];
                    }
                }
                
            }];    
        }
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint offset;
    if(textField==self->txtOTP)
    {
        offset=CGPointMake(0, 200);
        [self.scrollView setContentOffset:offset animated:YES];
    }
    //self.viewForReferralError.hidden=YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [timerForOTPExpiry invalidate];
    timerForOTPExpiry = nil;
}



- (IBAction)onClickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
