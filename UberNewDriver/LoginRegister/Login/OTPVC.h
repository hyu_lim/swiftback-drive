
#import <UIKit/UIKit.h>

@interface OTPVC : UIViewController<UITextFieldDelegate>
{
    __weak IBOutlet UILabel *lblMobileNo;
    
    __weak IBOutlet UITextField *txtOTP;
    
    NSMutableDictionary *dictLogin;
    
    NSTimer *timerForOTPExpiry;
    
    int secondsLeft;
    
    int minutes, seconds;
    
    NSUserDefaults *defaults;

    
}
@property (weak, nonatomic) IBOutlet UILabel *lblTimer;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
- (IBAction)onClickResend:(id)sender;
- (IBAction)onClickSubmit:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)onClickBack:(id)sender;
@end
