
#import "ShareVC.h"
#import "Social/Social.h"
#import "supportVC.h"
#import "SWRevealViewController.h"

@interface ShareVC ()

@end

@implementation ShareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle: ) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
    
    [mailComposer setDelegate:self];

    
    
    //rectangle background Rect
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    // rectangle background view
    _backgroundView.layer.cornerRadius = 10;
    _backgroundView.layer.borderWidth = 1;
    _backgroundView.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundView.layer.shadowRadius = 5.0;
    _backgroundView.layer.shadowOpacity = 0.4;
    
    // btnFacebook
    _btnFacebook.layer.cornerRadius = 5;
    _btnFacebook.layer.borderWidth = 0.1;
    _btnFacebook.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // btnWhatsapp
    _btnWhatsapp.layer.cornerRadius = 5;
    _btnWhatsapp.layer.borderWidth = 0.1;
    _btnWhatsapp.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // btnMail
    _btnMail.layer.cornerRadius = 5;
    _btnMail.layer.borderWidth = 0.1;
    _btnMail.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    //blurView
    _blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //NSLog(@"Result: canceled");
            break;
        case MFMailComposeResultSaved:
            //NSLog(@"Result: saved");
            break;
        case MFMailComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RESULT", nil) message:NSLocalizedString(@"MAIL_SENT_SUCCESSFULLY", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Result: failed");
            break;
        default:
            //NSLog(@"Result: not sent");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onClickFacebook:(id)sender {
    
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:NSLocalizedString(@"RIDE_SHARING_APP", nil)];
        [controller addURL:[NSURL URLWithString:@"http://www.swiftback.com"]];
        [controller addImage:[UIImage imageNamed:@"socialsharing-facebook-image-01.jpg"]];
        
        [self presentViewController:controller animated:YES completion:Nil];
    }
}

- (IBAction)onClickTwitter:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:NSLocalizedString(@"RIDE_SHARING_APP_WITH_LINK", nil)];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }

}

- (IBAction)onClickMsg:(id)sender {
    
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:NSLocalizedString(@"CHECK_THIS_OUT", nil)];
    [mailComposer setMessageBody:NSLocalizedString(@"RIDE_SHARING_APP_WITH_LINK", nil) isHTML:YES];
    //[mailComposer setMessageBody:@"Testing message for the test mail" isHTML:NO];
    [self presentViewController:mailComposer animated:YES completion:nil];
    
}

- (IBAction)onClickWhatsapp:(id)sender {
    NSString *message = @"Check out this new app. SwiftBack ride sharing app for an affordable and greener ride. http://www.swiftback.com";
    NSString *url = [NSString stringWithFormat:@"whatsapp://send?text=%@",message];
    NSURL *whatsappURL = [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if([[UIApplication sharedApplication] canOpenURL:whatsappURL]) {
        [[UIApplication sharedApplication] openURL:whatsappURL];
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"WhatsApp not installed." message:@"Your device hasn't installed WhatsApp." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end

