//
//  ScrollTutorialViewController.m
//  SwiftBack Driver
//
//  Created by Zin Mar Htet on 13/2/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "ScrollTutorialViewController.h"

@interface ScrollTutorialViewController ()

@end

@implementation ScrollTutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"ScrollTutorialViewController");
    [self setupScrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void) setupScrollView {
    
    //add scrollview to the view
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    _scrollView.bounces = NO;
    [_scrollView setAlwaysBounceVertical:NO];
    
    //setup internal views
    NSInteger numberOfViews = 4;
    
    for(int i = 0; i < numberOfViews; i++) {
        CGFloat xOrigin = i * self.view.frame.size.width;
        //Add ImageView
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //        image.image = [UIImage imageNamed:[NSString stringWithFormat:@"page_%d",i+1]];
        image.image = [UIImage imageNamed:[_pageImages objectAtIndex:i]];
        image.contentMode = UIViewContentModeScaleAspectFit;
        [_scrollView addSubview:image];
        
        //Add Label
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(xOrigin + 50, 180, 200, 30)];
        lblTitle.text = [_pageLabel objectAtIndex:i];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.backgroundColor = [UIColor whiteColor];
        [lblTitle setFont:[UIFont fontWithName:@"Arial" size:20.0f]];
        lblTitle.font = [UIFont boldSystemFontOfSize:20.0f];
        [lblTitle setTextColor:[UIColor darkGrayColor]];
        [_scrollView addSubview:lblTitle];
        
        UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(xOrigin + 50, 400, 200, 30)];
        [backBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [backBtn setTitle:@"Back" forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backToTutorialListPage) forControlEvents:UIControlEventTouchUpInside];
        [_scrollView addSubview:backBtn];
    }
    
    //    _tips = [UIImageView new];
    //    _tips.backgroundColor = [UIColor colorWithRed:0.9 green:0.1 blue:0.2 alpha:0.4];
    //    [self.view addSubview:_tips];
    //    _tips.frame = CGRectMake(80, 200, 80, 80);
    //    _tips.layer.cornerRadius = 80/2;
    //    [_scrollView addSubview:_tips];
    
    //set the scroll view content size
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width * numberOfViews, self.view.frame.size.height - 30);
    //add the scroll view to this view
    [self.view addSubview:_scrollView];
}

- (void)backToTutorialListPage {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone"
                                                         bundle:nil];
    AboutTutorialTableViewController *aboutTutorialVC =
    (AboutTutorialTableViewController *)
    [storyboard instantiateViewControllerWithIdentifier:@"AboutTutorialTableViewController"];
    [self.navigationController pushViewController:aboutTutorialVC animated:YES];
}

@end
