//
//  CreateOfferVC.m
//  SwiftBack Driver
//
//  Created by Elluminati on 18/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "CreateOfferVC.h"
#import "SWRevealViewController.h"

@interface CreateOfferVC ()

@end

@implementation CreateOfferVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self getUserLocation];
    [self customSetup];
    [self layoutSetup];
}

- (void)layoutSetup {
    
    [datepicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    
    SEL selector = NSSelectorFromString(@"setHighlightsToday:");
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDatePicker instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:datepicker];
    
    [datepicker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:15*60]];
    [datepicker setMaximumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*7]];
    
    //    [datepicker addTarget:self
    //                          action:@selector(setMinimumDateForFutureRequest)
    //                forControlEvents:UIControlEventValueChanged];
    

    CLLocationCoordinate2D current;
    current.latitude=[struser_lati doubleValue];
    current.longitude=[struser_longi doubleValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:current.latitude
                                                            longitude:current.longitude
                                                                 zoom:15];
    mapView_ = [GMSMapView mapWithFrame:viewForGoogleMap.bounds camera:camera];
    mapView_.myLocationEnabled = NO;
    [viewForGoogleMap addSubview:mapView_];
    mapView_.delegate=self;
    
    driverMarker = [[GMSMarker alloc] init];
    sourceMarker = [[GMSMarker alloc] init];
    destinationMarker = [[GMSMarker alloc] init];

    //blurViewForAddress
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.blurViewForAddress.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.blurViewForAddress.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.blurViewForAddress addSubview:blurEffectView];
    }
    else {
        self.blurViewForAddress.backgroundColor = [UIColor blackColor];
    }
    
    //viewForEstimate
    viewForEstimate.layer.cornerRadius = 10;
    viewForEstimate.clipsToBounds = YES;
    viewForEstimate.layer.shadowRadius = 4.0;
    viewForEstimate.layer.shadowColor = [UIColor grayColor].CGColor;
    viewForEstimate.layer.shadowOpacity = 0.7;

    //viewForOfferInfo
    viewForOfferInfo.layer.cornerRadius = 10;
    viewForOfferInfo.clipsToBounds = YES;
    
    //estimateNextBtn
    estimateNextBtn.layer.cornerRadius = 19;
    estimateNextBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    estimateNextBtn.layer.borderWidth = 1;
    
    //viewForDate
    viewForDate.layer.cornerRadius = 10;
    viewForDate.clipsToBounds = YES;
    viewForDate.layer.shadowRadius = 4.0;
    viewForDate.layer.shadowColor = [UIColor grayColor].CGColor;
    viewForDate.layer.shadowOpacity = 0.7;
    
    //viewForDatePicker
    viewForDatePicker.layer.cornerRadius = 10;
    viewForDatePicker.clipsToBounds = YES;
    
    //datePicker
    [datepicker setValue:[UIColor whiteColor] forKey:@"textColor"];
    
    //dateNextBtn
    dateNextBtn.layer.cornerRadius = 19;
    dateNextBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    dateNextBtn.layer.borderWidth = 1;
    dateNextBtn.clipsToBounds = YES;

    //viewForNotes
    viewForNotes.layer.cornerRadius = 10;
    viewForNotes.clipsToBounds = YES;
    viewForNotes.layer.shadowRadius = 4.0;
    viewForNotes.layer.shadowColor = [UIColor grayColor].CGColor;
    viewForNotes.layer.shadowOpacity = 0.7;
    
    //viewForNoOfPassenger
    viewForNoOfPassenger.layer.cornerRadius = 10;
    viewForNoOfPassenger.clipsToBounds = YES;
    
    //noteNextBtn
    noteNextBtn.layer.cornerRadius = 19;
    noteNextBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    noteNextBtn.layer.borderWidth = 1;
    noteNextBtn.clipsToBounds = YES;
    
    //txtNotes
    txtNotes.layer.cornerRadius = 15;
    txtNotes.clipsToBounds = YES;
    txtNotes.layer.borderColor = [UIColor whiteColor].CGColor;
    txtNotes.layer.borderWidth = 1;
    
    //viewForConfirm
    viewForConfirm.layer.cornerRadius = 10;
    viewForConfirm.clipsToBounds = YES;
    viewForConfirm.layer.borderWidth = 1;
    viewForConfirm.layer.borderColor = [UIColor lightGrayColor].CGColor;
    viewForConfirm.layer.shadowRadius = 4.0;
    viewForConfirm.layer.shadowColor = [UIColor grayColor].CGColor;
    viewForConfirm.layer.shadowOpacity = 0.7;
    
    //confirmOfferBtn
    confirmOfferBtn.layer.cornerRadius = 19;
    confirmOfferBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    confirmOfferBtn.layer.borderWidth = 1;
    confirmOfferBtn.clipsToBounds = YES;
}

//menuBtn
-(void)customSetup{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //Swipe to reveal menu
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

//backBtn
- (IBAction)onClickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)reloadDatePicker
{
    [datepicker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:15*60]];
    [datepicker setMaximumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*7]];
    [datepicker reloadInputViews];
}

#pragma mark-
#pragma mark- Get Location

-(void)getUserLocation
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        struser_lati=[NSString stringWithFormat:@"%.8f",currentLocation.coordinate.latitude];//[NSString stringWithFormat:@"%.8f",+37.40618700];
        struser_longi=[NSString stringWithFormat:@"%.8f",currentLocation.coordinate.longitude];//[NSString stringWithFormat:@"%.8f",-122.18845228];
    }
    
//    CLLocationCoordinate2D current;
//    current.latitude=[struser_lati doubleValue];
//    current.longitude=[struser_longi doubleValue];
//    [mapView_ animateToLocation:current];
    
    
    driverMarker.position = currentLocation.coordinate;
    driverMarker.icon = [UIImage imageNamed:@"pin_driver"];
    driverMarker.map = mapView_;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Tableview Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if([placeMarkArr count]>0)
    {
        NSString *formatedAddress=[[placeMarkArr objectAtIndex:indexPath.row] valueForKey:@"description"]; // AUTOCOMPLETE API
        
        // cell.lblTitle.text=currentPlaceMark.name;
        cell.textLabel.text = formatedAddress;
        
        cell.textLabel.font = [UberStyleGuide fontRegular];
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *aPlacemark=[placeMarkArr objectAtIndex:indexPath.row];
    tableForPlaces.hidden=YES;
    
    if([isSource isEqualToString:@"1"])
    {
        isList = YES;
        placeID = [aPlacemark objectForKey:@"place_id"];
        
        txtSource.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        
         [self textFieldShouldReturn:txtSource];
        
        [self searchLocationByPlaceID];
    }
    else if([isSource isEqualToString:@"2"])
    {
        placeID = [aPlacemark objectForKey:@"place_id"];
        isList = YES;
        
        txtDestination.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:txtDestination];
        
        [self searchLocationByPlaceID];

    }
    
    // [self textFieldShouldReturn:nil];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return placeMarkArr.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// Address Events
-(IBAction)onClickSearch:(UITextField*)sender{
    
    [placeMarkArr removeAllObjects];
    tableForPlaces.hidden=YES;
    
    NSString *address;
    
    if (sender == txtSource) {
        address = txtSource.text;
        isSource = @"1";
    }
    else{
        address = txtDestination.text;
        isSource = @"2";
    }
    
    
    if(address == nil)
        tableForPlaces.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    //[dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:address forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    [dictParam setObject:GOOGLE_KEY forKey:@"key"];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             //NSArray *arrAddress=[response valueForKey:@"results"];
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             
             if ([arrAddress count] > 0)
             {
                 tableForPlaces.hidden=NO;
                 
                 placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                 //[placeMarkArr addObject:Placemark]; o
                 [tableForPlaces reloadData];
                 
                 if(arrAddress.count==0)
                 {
                     tableForPlaces.hidden=YES;
                 }
             }
             
         }
         
     }];
    
}


-(void)searchLocationByPlaceID{
    
    [APPDELEGATE showLoadingWithTitle:@"Loading"];
    AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [helper getLocationFromPlaceIdwithParamData:placeID withBlock:^(id response, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        if (response)
        {
            if ([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
                NSDictionary *dictLocation=[[[response valueForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"];
                
                if ([isSource isEqualToString:@"1"]) {
                    
                    sourceLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                    sourceLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                    
                    sourcecoordinate.latitude=[sourceLatitude doubleValue];
                    sourcecoordinate.longitude=[sourceLongitude doubleValue];
                    
                        sourceMarker.position = sourcecoordinate;
                        sourceMarker.icon=[UIImage imageNamed:@"pin_client_org"];
                        sourceMarker.map = mapView_;
                    
                    [mapView_ animateToLocation:sourcecoordinate];
                    
                }
                else{
                    destinationLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                    destinationLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                    
                    destinationcoordinate.latitude=[destinationLatitude doubleValue];
                    destinationcoordinate.longitude=[destinationLongitude doubleValue];
                    
                    destinationMarker.position = destinationcoordinate;
                    destinationMarker.icon=[UIImage imageNamed:@"pin_destination"];
                    destinationMarker.map = mapView_;
                    
                    [self getDistanceAndCost];
                }
            }
        }
    }];
}

-(void)getDistanceAndCost
{
    if ([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Please wait..."];
        NSMutableDictionary *dictparam = [[NSMutableDictionary alloc]init];
        [dictparam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictparam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictparam setObject:sourceLatitude forKey:PARAM_SOURCE_LATITUDE];
        [dictparam setObject:sourceLongitude forKey:PARAM_SOURCE_LONGITUDE];
        [dictparam setObject:destinationLatitude forKey:PARAM_DESTIANTION_LATITUDE];
        [dictparam setObject:destinationLongitude forKey:PARAM_DESTIANTION_LONGITUDE];
        
   
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_GET_USER_INFO withParamData:dictparam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response)
            {
                if ([[response valueForKey:@"success"] boolValue])
                {
                    NSDictionary *details = [response valueForKey:@"details"];
                    
                    if ([[details valueForKey:@"distance"] integerValue] > 0) {
                        [lblEstimateDistance setText:[NSString stringWithFormat:@"%.2f Km",[[details valueForKey:@"distance"] doubleValue]]];
                        [lblOfferPrice setText:[NSString stringWithFormat:@"$%@",[details valueForKey:@"total"]]];
                        distance = [NSString stringWithFormat:@"%@",[details valueForKey:@"distance"]];
                        [viewForEstimate setHidden:NO];
                        cost = [NSString stringWithFormat:@"%@",[details valueForKey:@"total"]];
                         [self DrawPath:sourcecoordinate to:destinationcoordinate];
                        
                    }
                    
                    else{
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"NOT_VALID_LOCATION", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alert show];
                    }
                }
                else{
                    if([response valueForKey:@"error"]){
                        
                        if ([[response valueForKey:@"error"] isEqualToString:@"User Not Found"] || [[response valueForKey:@"error"] isEqualToString:@"Not a valid token"]) {
                            [USERDEFAULT setBool:NO forKey:PREF_IS_LOGIN];
                            [USERDEFAULT removeObjectForKey:PREF_LOGIN_OBJECT];
                            [USERDEFAULT synchronize];
                            [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login expired" message:@"Please relogin." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                            [alert show];                        }
                        else{
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                            [alert show];
                        }
                        
                    }
                    
                }
            }
            else
            {
                
                [viewForEstimate setHidden:YES];
                [txtDestination setText:@""];
                destinationLatitude = @"";
                destinationLongitude = @"";
                destinationMarker = [[GMSMarker alloc]init];
                [txtDestination becomeFirstResponder];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please try again." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                
                
                
                
            }
            
        }];
    }
}

#pragma mark-
#pragma mark- Show Route With Google

-(void)DrawPath:(CLLocationCoordinate2D)f to:(CLLocationCoordinate2D)t
{
    [APPDELEGATE showLoadingWithTitle:@"loading"];
    [mapView_ clear];
    

    GMSMarker *markerOwner = [[GMSMarker alloc] init];
    markerOwner.position = f;
    markerOwner.icon = [UIImage imageNamed:@"pin_client_org"];
    markerOwner.map = mapView_;
    
    GMSMarker *markerDriver = [[GMSMarker alloc] init];
    markerDriver.position = t ;
    
    markerDriver.icon = [UIImage imageNamed:@"pin_destination"];
    markerDriver.map = mapView_;
    
    routes = [self calculateRoute:f to:t];
    
    [self centerMap:routes];
    [APPDELEGATE hideLoadingView];
}

-(NSArray*)calculateRoute:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY];
    //chk
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] ||  [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
    {
    }
    else
    {
        GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
        GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 4.0f;
        singleLine.strokeColor = [UIColor colorWithRed:27.0/255.0 green:151.0/255.0 blue:200.0/255.0 alpha:1.0];
        singleLine.map = mapView_;
        
        routes = json[@"routes"];
        
        
    }
    NSString *points = @"";
    if ([routes count] > 0)
    {
        points=[[[routes objectAtIndex:0] objectForKey:@"overview_polyline"] objectForKey:@"points"];
        
    }
    
    return [self decodePolyLine:[points mutableCopy]];
}

-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded {
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init] ;
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude=[[NSNumber alloc] initWithFloat:lat *1e-5];
        NSNumber *longitude=[[NSNumber alloc] initWithFloat:lng *1e-5];
        
        printf("[%f,", [latitude doubleValue]);
        printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]] ;
        [array addObject:loc];
    }
    
    return array;
}

-(void)centerMap:(NSArray*)locations
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocationCoordinate2D location;
    for (CLLocation *loc in locations)
    {
        location.latitude = loc.coordinate.latitude;
        location.longitude = loc.coordinate.longitude;
        // Creates a marker in the center of the map.
        bounds = [bounds includingCoordinate:location];
    }
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:20.0f]];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}

// Estimate View events

- (IBAction)onClickEstimateNext:(id)sender{
    
    [viewForEstimate setHidden:YES];
    [viewForDate setHidden:NO];
    [self reloadDatePicker];
}

- (IBAction)onClickMinus:(id)sender{
    
    if ([cost doubleValue] > 0) {
        
        cost = [NSString stringWithFormat:@"%.1f",[cost doubleValue] - 0.5];
        [lblOfferPrice setText:[NSString stringWithFormat:@"$%@",cost]];
        
    }
}

- (IBAction)onClickPlus:(id)sender{
	
	if ([cost doubleValue] < 40) {
        
		cost = [NSString stringWithFormat:@"%.1f",[cost doubleValue] + 0.5];
		[lblOfferPrice setText:[NSString stringWithFormat:@"$%@",cost]];
	}
}

// Date View Events
- (IBAction)onClickDateNext:(id)sender{
    
    [viewForDate setHidden:YES];
    [viewForNotes setHidden:NO];
}

- (IBAction)goBackToEstimate:(id)sender{
    
    [viewForEstimate setHidden:NO];
    [viewForDate setHidden:YES];
}

// Note View Events
- (IBAction)onClickPassengerStepper:(id)sender{
    
    [lblPassengerCount setText:[NSString stringWithFormat:@"%.f",passengerStepper.value]];
}

- (IBAction)onClickNotesNext:(id)sender{
    
    [viewForNotes setHidden:YES];
    [viewForConfirm setHidden:NO];
    [viewForGreenOverlay setHidden:NO];
    
    [lblFrom setText:txtSource.text];
    [lblTo setText:txtDestination.text];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"SGT"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//2013-07-15:10:00:00
    strFutureDate = [formatter stringFromDate:datepicker.date];
    
    NSString *strDate = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"dd MMM"];
    NSString *strTime  = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"HH:mm a"];

    [lblDate setText:strDate];
    [lblTime setText:strTime];
    [lblDistance setText:lblEstimateDistance.text];
    [lblPrice setText:lblOfferPrice.text];
    [lblNotes setText:txtNotes.text];
//    [lblSeats setText:lblPassengerCount.text];
    if([lblPassengerCount.text isEqualToString:@"1"]) {
        [lblSeats setText:[NSString stringWithFormat:@"%@ Seat",lblPassengerCount.text]];
    } else {
        [lblSeats setText:[NSString stringWithFormat:@"%@ Seats",lblPassengerCount.text]];
    }
}

- (IBAction)goBackToDateView:(id)sender{
    
    [viewForDate setHidden:NO];
    [viewForNotes setHidden:YES];
    [self reloadDatePicker];
    
}

// Confirm Offer Events
- (IBAction)onClickEditOffer:(id)sender{
    
    [viewForConfirm setHidden:YES];
    [viewForGreenOverlay setHidden:YES];
    [viewForNotes setHidden:NO];
}

- (IBAction)onClickConfirmOffer:(id)sender{
    
    if ([APPDELEGATE connected]) {
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:sourceLatitude forKey:PARAM_LATITUDE];
        [dictParam setObject:sourceLongitude  forKey:PARAM_LONGITUDE];
        [dictParam setObject:destinationLatitude forKey:PARAM_DESTIANTION_LATITUDE];
        [dictParam setObject:destinationLongitude  forKey:PARAM_DESTIANTION_LONGITUDE];
        [dictParam setObject:txtSource.text forKey:PARAM_SOURCE_ADDRESS];
        [dictParam setObject:txtDestination.text forKey:PARAM_DESTIANTION_ADDRESS];
        [dictParam setObject:strFutureDate forKey:DATE_TIME];
        [dictParam setObject:distance forKey:PARAM_DISTANCE];
        [dictParam setObject:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:cost forKey:PARAM_TOTAL];
        
        [dictParam setObject:[NSString stringWithFormat:@"%d",(int)passengerStepper.value] forKey:PARAM_NO_OF_PASSENGER];
        [dictParam setObject:txtNotes.text forKey:PARAM_NOTE];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_OFFER_RIDE withParamData:dictParam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response) {
                if ([[response objectForKey:@"success"] boolValue]) {
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Offer created successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
					
					if ([self.delegate respondsToSelector:@selector(rideOfferCreated)]) {
						[self.delegate rideOfferCreated];
					}
					
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error occured" message:@"Please try again." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
    } else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

@end
