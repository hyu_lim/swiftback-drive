
#import "ProfileVC.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "UtilityClass.h"
#import "PickMeUpMapVC.h"
#import "ArrivedMapVC.h"
#import "FeedBackVC.h"
#import "UINavigationBar+Awesome.h"

@interface ProfileVC ()
{
    BOOL internet;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableString *strPassword;
    CGFloat savingAmount;
    CGFloat goalAmount;
    BOOL isApproved;
}

@end

@implementation ProfileVC

@synthesize txtAddress,title,txtBio,txtEmail,txtLastName,txtName,txtNumber,txtZip,txtPassword,btnProPic,txtNewPassword,bgNewPwd,bgPwd,txtTaxiModel,txtTaxiNumber,txtReNewPassword,bgReNewPwd,txtCarColor,userName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    [super setBackBarItem];
    [self customSetup];
    [self layoutSetup];
    
    //UITapGestureRecognizer setup
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideUpAndDownWithTapGestureRecognizer:)];
    
    [self.viewForEarnings addGestureRecognizer:singleTapGestureRecognizer];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.lblCarModelInfo.hidden=YES;
    self.imgCarModelInfo.hidden=YES;
    self.lblCarNumberInfo.hidden=YES;
    self.imgCarNumberInfo.hidden=YES;
    self.lblEmailInfo.hidden=YES;
    self.imgEmailInfo.hidden=YES;
    self.btnInfo1.tag=0;
    self.btnInfo2.tag=0;
    self.btnInfo3.tag=0;
    self.viewResetPwd.hidden=YES;
}


- (void)layoutSetup {
    
    [self.profileImage applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [self.ScrollProfile setScrollEnabled:YES];
    [self.ScrollProfile setContentSize:CGSizeMake(320, 440)];
    
    [self textDisable];
    //[self localizeString];
    
    [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle: ) forControlEvents:UIControlEventTouchUpInside];
    [self.viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref synchronize];
    strUserId=[pref objectForKey:PREF_USER_ID];
    strUserToken=[pref objectForKey:PREF_USER_TOKEN];
    strPassword=[pref objectForKey:PREF_PASSWORD];
    
    [txtName setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    [txtLastName setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    [txtNumber setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    
    [txtTaxiNumber setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    [txtTaxiModel setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    [txtCarColor setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    
    NSLog(@"ArrUser : %@", arrUser);
    
    txtName.text=[arrUser valueForKey:@"first_name"];
    userName.text=[arrUser valueForKey:@"first_name"];
    txtLastName.text=[arrUser valueForKey:@"last_name"];
    txtEmail.text=[arrUser valueForKey:@"email"];
    txtNumber.text=[arrUser valueForKey:@"phone"];
    txtAddress.text=[arrUser valueForKey:@"address"];
    txtZip.text=[arrUser valueForKey:@"zipcode"];
    txtBio.text=[arrUser valueForKey:@"bio"];
    txtTaxiModel.text=[arrUser valueForKey:@"car_type"];
    txtTaxiNumber.text=[arrUser valueForKey:@"car_number"];
    txtCarColor.text=[arrUser valueForKey:@"car_color"];
    isApproved = [[arrUser valueForKey:@"is_approved"] boolValue];
    //    NSLog(@"Is Approved : %d",isApproved);
    
    // 1 is approved, 0 is not approved
    if ([[arrUser valueForKey:@"is_approved"] boolValue ]) {
        _documentBtn.hidden = YES;
        _btnResetPassword.frame = CGRectMake(_btnResetPassword.frame.origin.x, _btnResetPassword.frame.origin.y - 45, _btnResetPassword.frame.size.width, _btnResetPassword.frame.size.height);
        NSLog(@"Approved : %d",isApproved);
    } else {
        _documentBtn.hidden = NO;
        NSLog(@"Not Approved : %d",isApproved);
    }
    
    txtPassword.text=@"";
    txtNewPassword.text=@"";
    
    [self.ratingView initRateBar];
    [self.ratingView setUserInteractionEnabled:NO];
    //RBRatings rate=([[arrUser valueForKey:@"rating"]floatValue]*2);
//    RBRatings rate = 5;
//    [self.ratingView setRatings:rate];
    
    [self.profileImage downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:nil];
    [self.profileBackground downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:nil];
    
    [self.btnUpdate setHidden:YES];
    // Do any additional setup after loading the view.
    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    //    _backgroundRect.layer.shadowRadius = 5.0;
    //    _backgroundRect.layer.shadowOpacity = 0.4;
    
    
    //rectangle background
    //    _backgroundVehicleInfo.layer.cornerRadius = 10;
    _backgroundVehicleInfo.layer.borderWidth = 0.7;
    _backgroundVehicleInfo.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //    _backgroundVehicleInfo.layer.shadowRadius = 5.0;
    //    _backgroundVehicleInfo.layer.shadowOpacity = 0.4;
    
    //rectangle background Reset
    _backgroundReset.layer.cornerRadius = 10;
    _backgroundReset.layer.borderWidth = 1;
    _backgroundReset.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundReset.layer.shadowRadius = 5.0;
    _backgroundReset.layer.shadowOpacity = 0.4;
    
    //rectangle background Reset
    _viewForEarnings.layer.cornerRadius = 10;
    _viewForEarnings.layer.borderWidth = 1.5
    ;
    _viewForEarnings.layer.borderColor = [UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1].CGColor;
    _viewForEarnings.layer.shadowRadius = 3.0;
    _viewForEarnings.layer.shadowOpacity = 0.3;
    
    //rectangle background Monthly Goal
    _viewForMonthlyGoal.layer.cornerRadius = 10;
    _viewForMonthlyGoal.layer.borderWidth = 1;
    _viewForMonthlyGoal.layer.borderColor = [UIColor whiteColor].CGColor;
    _viewForMonthlyGoal.layer.shadowRadius = 5.0;
    _viewForMonthlyGoal.layer.shadowOpacity = 0.4;
    
    //rectangle background Monthly Goal
    _viewResetPwd.layer.cornerRadius = 10;
    _viewResetPwd.layer.borderWidth = 1;
    _viewResetPwd.layer.borderColor = [UIColor whiteColor].CGColor;
    _viewResetPwd.layer.shadowRadius = 5.0;
    _viewResetPwd.layer.shadowOpacity = 0.4;
    
    // documentBtn
    _documentBtn.layer.cornerRadius = 5;
    _documentBtn.layer.borderWidth = 0.1;
    _documentBtn.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)customSetup {
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [self getSavings];
}

//Progress View methods
- (void)showCurrentSavingWithPV{
    NSLog(@"showCurrentSavingWithPV");
    NSLog(@"Goal Amount : %f", savingAmount);
    NSLog(@"Goal Amount : %f", goalAmount);
    CGFloat progressValue = (savingAmount / goalAmount);
    NSLog(@"Progress Value : %f", progressValue);

    _lblMonthlyGoal.text = [NSString stringWithFormat:@"$%d", (int)goalAmount];
    _pvSaving.progress = progressValue;
    
}

- (IBAction)saveMonthlyGoal:(id)sender {
    
    if ([self.txtMontlyGoal.text integerValue] > 0) {
        _viewForGreenOverlay.alpha = 0;
        _viewForMonthlyGoal.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            _viewForGreenOverlay.alpha = 0.8;
            _viewForMonthlyGoal.alpha = 1;
            [_viewForGreenOverlay setHidden: NO];
            [_viewForMonthlyGoal setHidden: NO];
            CGPoint offset;
            offset=CGPointMake(0, 0);
            [self.ScrollProfile setContentOffset:offset animated:YES];
            [_txtMontlyGoal resignFirstResponder];
            [self submitGoal];
        }];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Invalid Goal!" message:@"Please enter valid amount" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

- (IBAction)setMonthlyGoal:(id)sender {
    
        _viewForGreenOverlay.alpha = 0;
        _viewForMonthlyGoal.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            _viewForGreenOverlay.alpha = 0.8;
            _viewForMonthlyGoal.alpha = 1;
            [_viewForGreenOverlay setHidden: NO];
            [_viewForMonthlyGoal setHidden: NO];
            CGPoint offset;
            offset=CGPointMake(0, 0);
            [self.ScrollProfile setContentOffset:offset animated:YES];
            [_txtMontlyGoal resignFirstResponder];
        }];
   }

- (IBAction)cancelMonthlyGoal:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewForGreenOverlay.alpha = 0;
        _viewForMonthlyGoal.alpha = 0;
    } completion:^(BOOL finished){
        [_viewForGreenOverlay setHidden: finished];
        [_viewForMonthlyGoal setHidden: finished];
    }];
}

-(void)getSavings{
    
    if ([APPDELEGATE connected]) {
        
        [APPDELEGATE showLoadingWithTitle:@"Loading"];
        
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_GET_SAVINGS withParamData:dictParam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response) {
                if ([[response valueForKey:@"success"] boolValue]) {
                    [self.viewForEarnings setHidden:NO];
                    [self.lblSavingOfMonth setText:[NSString stringWithFormat:@"$%@",[response valueForKey:@"monthly_saving"]]];
                    [self.lblGrossSavings setText:[NSString stringWithFormat:@"$%@",[response valueForKey:@"gross_saving"]]];
                    
                    NSString *goal = [NSString stringWithFormat:@"%@",[response valueForKey:@"my_goal"]];

                    savingAmount = [[NSString stringWithFormat:@"%@",[response valueForKey:@"monthly_saving"]] floatValue];
                    
                    if(![goal isEqualToString:@""]) {
                        goalAmount = [[NSString stringWithFormat:@"%@",[response valueForKey:@"my_goal"]] floatValue];
                    } else {
                        goalAmount = 0.0;
                    }
                    if ([response valueForKey:@"document_status"]) {
                        
                        NSInteger status = [[response valueForKey:@"document_status"] integerValue];
                        if (status == 0) {
                            [self.documentBtn setHidden:NO];
                            [self.documentBtn setTitle:@"Account Not verified, Verify now ->" forState:UIControlStateNormal];
                        }
                        else if(status == 1){
                            [self.documentBtn setHidden:NO];
                            [self.documentBtn setUserInteractionEnabled:NO];
                            [self.documentBtn setTitle:@"Verification Pending" forState:UIControlStateNormal];
                        }
                        else{
                            [self.documentBtn setHidden:YES];
                        }
                    }
                    
                    [self showCurrentSavingWithPV];
                    
                }
                else{
                    [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                    [self.viewForEarnings setHidden:YES];
                }
            }
            else{
                [self.viewForEarnings setHidden:YES];
            }
        }];
    }
}

-(void)submitGoal
{
    
    if ([APPDELEGATE connected]) {
        [APPDELEGATE showLoadingWithTitle:@"Setting your goal"];
        
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:self.txtMontlyGoal.text forKey:PARAM_AMOUNT];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_SUBMIT_GOAL withParamData:dictParam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response) {
                if ([[response objectForKey:@"success"]boolValue]) {
                    
                    NSString *goal = [NSString stringWithFormat:@"%@",[response valueForKey:@"my_monthly_goal"]];
                    
                    if(![goal isEqualToString:@""]) {
                        goalAmount = [[NSString stringWithFormat:@"%@",[response valueForKey:@"my_monthly_goal"]] floatValue];
                    } else {
                        goalAmount = 0.0;
                    }
                    [self showCurrentSavingWithPV];
                    
                    [self.txtMontlyGoal setText:@""];
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Goal updated successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                    
                    [self cancelMonthlyGoal:nil];
                }
                else{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Goal could not be updated, please try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];

                }
            }
        }];
        
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (void)viewDidAppear:(BOOL)animated
{
    //[self.btnMenu setTitle:NSLocalizedString(@"PROFILE", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark- Custom Font

-(void)customFont
{
    self.txtName.font=[UberStyleGuide fontRegular];
    self.txtLastName.font=[UberStyleGuide fontRegular];
    self.txtEmail.font=[UberStyleGuide fontRegular];
    self.txtAddress.font=[UberStyleGuide fontRegular];
    self.txtBio.font=[UberStyleGuide fontRegular];
    self.txtZip.font=[UberStyleGuide fontRegular];
    self.txtTaxiNumber.font=[UberStyleGuide fontRegular];
    
    self.btnMenu.titleLabel.font=[UberStyleGuide fontRegular:9.0f];
    self.btnEdit=[APPDELEGATE setBoldFontDiscriptor:self.btnEdit];
    self.btnUpdate=[APPDELEGATE setBoldFontDiscriptor:self.btnUpdate];
    
}

-(void)localizeString
{
    self.txtName.placeholder = NSLocalizedString(@"NAME", nil);
    self.txtLastName.placeholder = NSLocalizedString(@"LAST_NAME", nil);
    self.txtEmail.placeholder = NSLocalizedString(@"EMAIL", nil);
    self.txtNumber.placeholder = NSLocalizedString(@"NUMBER", nil);
    
    //Welcome , name label
    self.userName.text=NSLocalizedString(@"NAME", nil);
    
//    self.lableNa.placeholder = NSLocalizedString(@"NAME", nil);
    
    self.txtAddress.placeholder = NSLocalizedString(@"ADDRESS", nil);
    self.txtBio.placeholder = NSLocalizedString(@"BOI", nil);
    self.txtPassword.placeholder = NSLocalizedString(@"CURRENT_PASSWORD", nil);
    self.txtNewPassword.placeholder = NSLocalizedString(@"NEW_PASSWORD", nil);
    self.txtReNewPassword.placeholder = NSLocalizedString(@"CONFIRM_NEW_PASSWORD", nil);
    self.txtZip.placeholder = NSLocalizedString(@"ZIPCODE", nil);
    
    self.txtTaxiModel.placeholder = NSLocalizedString(@"TAXI_MODEL", nil);
    self.txtTaxiNumber.placeholder = NSLocalizedString(@"CAR_NUMBER", nil);
    
    
    self.lblEmailInfo.text = NSLocalizedString(@"THIS_FIELD_IS_NOT_EDITABLE", nil);
    self.lblCarNumberInfo.text = NSLocalizedString(@"THIS_FIELD_IS_NOT_EDITABLE", nil);
    self.lblCarModelInfo.text = NSLocalizedString(@"THIS_FIELD_IS_NOT_EDITABLE", nil);
    
    [self.btnEdit setTitle:NSLocalizedString(@"EDIT_PROFILE", nil) forState:UIControlStateNormal];
    [self.btnEdit setTitle:NSLocalizedString(@"EDIT_PROFILE", nil) forState:UIControlStateSelected];
    
    [self.btnUpdate setTitle:NSLocalizedString(@"UPDATE_PROFILE", nil) forState:UIControlStateNormal];
    [self.btnUpdate setTitle:NSLocalizedString(@"UPDATE_PROFILE", nil) forState:UIControlStateSelected];
}

#pragma mak-
#pragma mark- TextField Enable and Disable

-(void)textDisable
{
    txtName.enabled = NO;
    txtLastName.enabled = NO;
    txtEmail.enabled = NO;
    txtNumber.enabled = NO;
    txtAddress.enabled = NO;
    txtZip.enabled = NO;
    txtBio.enabled = NO;
    btnProPic.enabled=NO;
    txtTaxiNumber.enabled = NO;
    txtTaxiModel.enabled = NO;
    txtCarColor.enabled = NO;
    //[self.ScrollProfile setScrollEnabled:NO];
    
    CGPoint offset;
    offset=CGPointMake(0, 0);
    [self.ScrollProfile setContentSize:CGSizeMake(320, 850)];
    [self.ScrollProfile setContentOffset:offset animated:YES];
}

-(void)textEnable
{
    txtName.enabled = YES;
    txtLastName.enabled = YES;
    txtEmail.enabled = NO;
    txtNumber.enabled = YES;
    txtAddress.enabled = YES;
    txtZip.enabled = YES;
    txtBio.enabled = YES;
    btnProPic.enabled=YES;
    txtTaxiNumber.enabled = YES;
    txtTaxiModel.enabled = YES;
    txtCarColor.enabled = YES;
    
    [self.ScrollProfile setContentSize:CGSizeMake(320, 850)];
    //[self.ScrollProfile setScrollEnabled:YES];
}

-(void)updatePRofile
{
    NSLog(@"\n\n IN Update PRofile");
    
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"UPDATING_PROFILE", nil)];
    internet=[APPDELEGATE connected];
    
    if(internet)
    {
        NSMutableDictionary *dictparam;
        dictparam= [[NSMutableDictionary alloc]init];
        
        [dictparam setObject:txtName.text forKey:PARAM_FIRST_NAME];
        [dictparam setObject:userName.text forKey:PARAM_FIRST_NAME];
        [dictparam setObject:txtLastName.text forKey:PARAM_LAST_NAME];
        [dictparam setObject:txtEmail.text forKey:PARAM_EMAIL];
        [dictparam setObject:txtNumber.text forKey:PARAM_PHONE];
        [dictparam setObject:txtTaxiNumber.text forKey:PARAM_CAR_NUMBER];
        [dictparam setObject:txtCarColor.text forKey:PARAM_CAR_COLOR];
        [dictparam setObject:txtTaxiModel.text forKey:PARAM_TAXI_MODEL];
        
        
        //[dictparam setObject:strPassword forKey:PARAM_PASSWORD];
        
        //            [dictparam setObject:txtPassword.text forKey:PARAM_OLDPASSWORD];
        //            [dictparam setObject:txtNewPassword.text forKey:PARAM_NEWPASSWORD];
        //[dictparam setObject:strPassword forKey:PARAM_PASSWORD];
        //[dictparam setObject:txtAddress.text forKey:PARAM_ADDRESS];
        //[dictparam setObject:txtBio.text forKey:PARAM_BIO];
        //[dictparam setObject:txtZip.text forKey:PARAM_ZIPCODE];
        //[dictparam setObject:device_token forKey:PARAM_DEVICE_TOKEN];
        
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictparam setObject:@"" forKey:PARAM_PICTURE];
        
        UIImage *imgUpload = [[UtilityClass sharedObject]scaleAndRotateImage:self.profileImage.image];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_UPDATE_PROFILE withParamDataImage:dictparam andImage:imgUpload withBlock:^(id response, NSError *error)
         {
             
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     arrUser=response;
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"PROFILE_UPDATED", nil)];
                     txtName.text=[arrUser valueForKey:@"first_name"];
                     userName.text=[arrUser valueForKey:@"first_name"];
                     txtLastName.text=[arrUser valueForKey:@"last_name"];
                     txtEmail.text=[arrUser valueForKey:@"email"];
                     txtNumber.text=[arrUser valueForKey:@"phone"];
                     txtAddress.text=[arrUser valueForKey:@"address"];
                     txtZip.text=[arrUser valueForKey:@"zipcode"];
                     txtBio.text=[arrUser valueForKey:@"bio"];
                     txtTaxiNumber.text=[arrUser valueForKey:@"car_number"];
                     [self.profileImage downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:nil];
                     [self.profileBackground downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:nil];
                     
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"PROFILE_UPDATE_FAIL", nil) message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
                 [self.btnEdit setHidden:NO];
                 [self.btnUpdate setHidden:YES];
                 [self textDisable];
             }
             
             [APPDELEGATE hideLoadingView];
             
             NSLog(@"REGISTER RESPONSE --> %@",response);
         }];
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark-
#pragma mark- Button Method

- (IBAction)LogOutBtnPressed:(id)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    
    [pref synchronize];
    [pref removeObjectForKey:PARAM_REQUEST_ID];
    [pref removeObjectForKey:PARAM_SOCIAL_ID];
    [pref removeObjectForKey:PREF_EMAIL];
    [pref removeObjectForKey:PREF_LOGIN_BY];
    [pref removeObjectForKey:PREF_PASSWORD];
    [pref setBool:NO forKey:PREF_IS_LOGIN];
    
    [self.navigationController.navigationController    popToRootViewControllerAnimated:YES];
}

- (IBAction)editBtnPressed:(id)sender
{
    [self textEnable];
    [APPDELEGATE showToastMessage:NSLocalizedString(@"YOU_CAN_EDIT_YOUR_PROFILE", nil)];
    [self.btnEdit setHidden:YES];
    [self.btnUpdate setHidden:NO];
    [txtName becomeFirstResponder];
}

- (IBAction)updateBtnPressed:(id)sender
{
    internet=[APPDELEGATE connected];
    if (self.txtNewPassword.text.length>=1 || self.txtReNewPassword.text.length>=1)
    {
        if ([txtNewPassword.text isEqualToString:txtReNewPassword.text])
        {
            [self updatePRofile];
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"PROFILE_UPDATE_FAIL", nil) message:NSLocalizedString(@"NOT_MATCH_RETYPE",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        [self updatePRofile];
    }
}

- (IBAction)backBtnPressed:(id)sender
{
    NSArray *currentControllers = self.navigationController.viewControllers;
    NSMutableArray *newControllers = [NSMutableArray
                                      arrayWithArray:currentControllers];
    UIViewController *obj=nil;
    
    for (int i=0; i<newControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if ([vc isKindOfClass:[FeedBackVC class]])
        {
            obj = (FeedBackVC *)vc;
        }
        else if ([vc isKindOfClass:[ArrivedMapVC class]])
        {
            obj = (ArrivedMapVC *)vc;
        }
        else if ([vc isKindOfClass:[PickMeUpMapVC class]])
        {
            obj = (PickMeUpMapVC *)vc;
        }
        
    }
    [self.navigationController popToViewController:obj animated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)imgPicBtnPressed:(id)sender
{
    UIActionSheet *action=[[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"PROFILE_UPDATE_FAIL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TAKE_PHOTO", nil), NSLocalizedString(@"SELECT_IMAGE", nil), nil];
    action.tag=10001;
    [action showInView:self.view];
}

- (IBAction)onClickEmailInfo:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==0)
    {
        btn.tag=1;
        self.lblEmailInfo.hidden=NO;
        self.imgEmailInfo.hidden=NO;
    }
    else
    {
        btn.tag=0;
        self.lblEmailInfo.hidden=YES;
        self.imgEmailInfo.hidden=YES;
    }
}

- (IBAction)onClickTaxiModelInfo:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==0)
    {
        btn.tag=1;
        self.lblCarModelInfo.hidden=NO;
        self.imgCarModelInfo.hidden=NO;
    }
    else
    {
        btn.tag=0;
        self.lblCarModelInfo.hidden=YES;
        self.imgCarModelInfo.hidden=YES;
    }
}

- (IBAction)onClickTaxiNoInfo:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==0)
    {
        btn.tag=1;
        self.lblCarNumberInfo.hidden=NO;
        self.imgCarNumberInfo.hidden=NO;
    }
    else
    {
        btn.tag=0;
        self.lblCarNumberInfo.hidden=YES;
        self.imgCarNumberInfo.hidden=YES;
    }
}

- (IBAction)onClickResetPwd:(id)sender
{
    _viewForGreenOverlay.alpha = 0;
    _viewResetPwd.alpha = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewForGreenOverlay.alpha = 0.8;
        self.viewResetPwd.alpha = 1;
        self.viewForGreenOverlay.hidden = NO;
        self.viewResetPwd.hidden = NO;
        CGPoint offset;
        offset=CGPointMake(0, 0);
        [self.ScrollProfile setContentOffset:offset animated:YES];
        [self.txtPassword resignFirstResponder];
        [self.txtNewPassword resignFirstResponder];
    }];
}

- (IBAction)onClickCancelPwd:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewForGreenOverlay.alpha = 0;
        _viewResetPwd.alpha = 0;
    } completion:^(BOOL finished){
        [_viewForGreenOverlay setHidden: finished];
        [_viewResetPwd setHidden: finished];
        CGPoint offset;
        offset=CGPointMake(0, 0);
        [self.ScrollProfile setContentOffset:offset animated:YES];
        [self.txtPassword resignFirstResponder];
        [self.txtNewPassword resignFirstResponder];
    }];
}

- (IBAction)onClickReset:(id)sender
{
    [self.txtPassword resignFirstResponder];
    [self.txtNewPassword resignFirstResponder];
    if([[AppDelegate sharedAppDelegate]connected])
    {
        if(self.txtNewPassword.text.length > 0 && self.txtPassword.text.length > 0)
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"PASSWORD_UPDATING", nil)];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setObject:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setObject:self.txtPassword.text forKey:@"old_password"];
            [dictParam setObject:self.txtNewPassword.text forKey:@"new_password"];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RESET_PASSWORD withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         self.viewForGreenOverlay.hidden = YES;
                         self.viewResetPwd.hidden=YES;
                         self.txtPassword.text=@"";
                         self.txtNewPassword.text=@"";
                         [self.txtNewPassword resignFirstResponder];
                         [self.txtPassword resignFirstResponder];
                         [[AppDelegate sharedAppDelegate] showToastMessage:[response valueForKey:@"message"]];
                     }
                     else
                     {
                         self.txtPassword.text=@"";
                         self.txtNewPassword.text=@"";
                         [[AppDelegate sharedAppDelegate] showToastMessage:[response valueForKey:@"message"]];
                     }
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"PLEASE_ENTER_OLD_AND_NEW_PWD_FOR_RESET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NETWORK_STAUS", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
    
}
- (IBAction)onClickPresentModal:(id)sender {
    
    [self performSegueWithIdentifier:@"segueToDocument" sender:nil];
}

#pragma mark -
#pragma mark - UIActionSheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self openCamera];
            break;
        case 1:
            [self chooseFromLibaray];
            break;
        case 2:
            break;
        case 3:
            break;
    }
}

-(void)openCamera
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        imagePickerController.delegate =self;
        imagePickerController.allowsEditing=YES;
        
        imagePickerController.view.tag = 102;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    }
    else
    {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"CAM_NOT_AVAILABLE", nil)delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alt show];
    }
}

-(void)chooseFromLibaray
{
    // Set up the image picker controller and add it to the view
    
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.delegate =self;
    
    imagePickerController.allowsEditing=YES;
    imagePickerController.view.tag = 102;
    
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:YES completion:^{
    }];
}

#pragma mark -
#pragma mark - UIImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.profileImage.image=[info objectForKey:UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark-
#pragma mark- Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField     //Hide the keypad when we pressed return
{
    CGPoint offset;
    offset=CGPointMake(0, 0);
    [self.ScrollProfile setContentOffset:offset animated:YES];
    /*if (textField==txtName)
     {
     [self.txtLastName becomeFirstResponder];
     }
     if (textField==txtLastName)
     {
     [self.txtEmail becomeFirstResponder];
     }
     if (textField==txtEmail)
     {
     [self.txtNumber becomeFirstResponder];
     }
     if (textField==txtNumber)
     {
     [self.txtAddress becomeFirstResponder];
     }
     if (textField==txtAddress)
     {
     [self.txtBio  becomeFirstResponder];
     }
     if (textField==txtBio)
     {
     [self.txtZip becomeFirstResponder];
     }*/
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    CGPoint offset;
    if(textField==self.txtEmail)
    {
        offset=CGPointMake(0, 0);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    }
    
    if(textField==self.txtNumber)
    {
        offset=CGPointMake(0, 40);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    }
    if(textField==self.txtAddress)
    {
        offset=CGPointMake(0, 80);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    }
    if(textField==self.txtBio)
    {
        offset=CGPointMake(0, 120);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    }
    if(textField==self.txtZip)
    {
        offset=CGPointMake(0, 160);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    }
    if(textField==self.txtPassword)
    {
        offset=CGPointMake(0, 60);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    }
    if(textField==self.txtNewPassword)
    {
        offset=CGPointMake(0, 100);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    }
    
    
    /*
     if(textField == self.txtPassword)
     {
     UITextPosition *beginning = [self.txtPassword beginningOfDocument];
     [self.txtPassword setSelectedTextRange:[self.txtPassword textRangeFromPosition:beginning
     toPosition:beginning]];
     [UIView animateWithDuration:0.3 animations:^{
     
     self.view.frame = CGRectMake(0, -35, 320, 480);
     
     } completion:^(BOOL finished) { }];
     }
     if(textField == self.txtAddress)
     {
     UITextPosition *beginning = [self.txtAddress beginningOfDocument];
     [self.txtAddress setSelectedTextRange:[self.txtAddress textRangeFromPosition:beginning
     toPosition:beginning]];
     [UIView animateWithDuration:0.3 animations:^{
     
     self.view.frame = CGRectMake(0, -75, 320, 480);
     
     } completion:^(BOOL finished) { }];
     }
     if(textField == self.txtBio)
     {
     UITextPosition *beginning = [self.txtBio beginningOfDocument];
     [self.txtBio setSelectedTextRange:[self.txtBio textRangeFromPosition:beginning
     toPosition:beginning]];
     [UIView animateWithDuration:0.3 animations:^{
     
     self.view.frame = CGRectMake(0, -115, 320, 480);
     
     } completion:^(BOOL finished) { }];
     }
     else if(textField == self.txtZip)
     {
     [UIView animateWithDuration:0.3 animations:^{
     
     self.view.frame = CGRectMake(0, -128, 320, 480);
     
     } completion:^(BOOL finished) { }];
     }
     */
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textField == self.txtPassword)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
            else if(textField == self.txtAddress)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
            else if(textField == self.txtBio)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
            else if (textField == self.txtZip)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
            
        }
        else
        {
            
            if(textField == self.txtPassword)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
            else if(textField == self.txtAddress)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
            else if(textField == self.txtBio)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
            else if (textField == self.txtZip)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
            
        }
    }
}

- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer {
    
    CGFloat newYCoordinate = 520.0;
    
    if(self.viewForEarnings.frame.origin.y == 520.0) {
        newYCoordinate = 370.0;
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        self.viewForEarnings.frame = CGRectMake(self.viewForEarnings.frame.origin.x, newYCoordinate, self.viewForEarnings.frame.size.width, self.viewForEarnings.frame.size.height);
    }];
}



@end
