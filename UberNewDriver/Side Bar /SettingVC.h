
#import "BaseVC.h"

@interface SettingVC : UITableViewController

@property (weak, nonatomic) IBOutlet UISwitch *swAvailable;
@property (weak, nonatomic) IBOutlet UILabel *lblYes;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailable;
@property (weak, nonatomic) IBOutlet UISwitch *swSound;
@property (weak, nonatomic) IBOutlet UILabel *lblSound;
@property (weak, nonatomic) IBOutlet UILabel *lblSoundtext;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)setState:(id)sender;
- (IBAction)setSound:(id)sender;

@end
