
#import "BaseVC.h"

@interface AboutVC : BaseVC <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollObj;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIWebView *webViewAbout;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIImageView *backgroudRect;
@property (weak, nonatomic) IBOutlet UIButton *btnTutorial;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *blurView;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)onClickTutorial:(id)sender;

@end
