//
//  AboutTutorialTableViewController.m
//  SwiftBack Driver
//
//  Created by Zin Mar Htet on 13/2/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "AboutTutorialTableViewController.h"

@interface AboutTutorialTableViewController ()

@end

@implementation AboutTutorialTableViewController
{
    NSMutableArray *arrTutorialList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"about tutorial tableviewcontroller");
    arrTutorialList = [[NSMutableArray alloc] initWithObjects:@"First Tutorial", @"Second Tutorial", @"Third Tutorial", nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showPageViewController"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ScrollTutorialViewController *destinationVC = segue.destinationViewController;
        
        NSLog(@"row no : %ld",(long)indexPath.row);
        
        if(indexPath.row == 0) {
            destinationVC.pageLabel = @[@"Over 200 Tips and Tricks", @"Discover Hidden Features", @"Bookmark Favorite Tip", @"Free Regular Update"];
            destinationVC.pageImages = @[@"page1.png", @"page2.png", @"page3.png", @"page4.png"];
        } else if (indexPath.row == 1) {
            destinationVC.pageLabel = @[@"Page2_01", @"Page2_02", @"Page2_03", @"Page2_04"];
            destinationVC.pageImages = @[@"page5.png", @"page6.png", @"page7.png", @"page8.png"];
        } else if(indexPath.row == 2) {
            destinationVC.pageLabel = @[@"Page3_01", @"Page3_02", @"Page3_03", @"Page3_04"];
            destinationVC.pageImages = @[@"Noa_1.jpeg", @"Noa_2.jpeg", @"Noa_3.jpeg", @"Noa_4.jpeg"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrTutorialList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = @"TutorialCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    UILabel *tutorialLabel = [cell viewWithTag:200];
    tutorialLabel.text = [arrTutorialList objectAtIndex:indexPath.row];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
