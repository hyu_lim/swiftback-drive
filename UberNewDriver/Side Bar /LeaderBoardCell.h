//
//  LeaderBoardCell.h
//  SwiftBack Driver
//
//  Created by Elluminati on 12/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaderBoardCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *driverProPic;

@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;

@property (weak, nonatomic) IBOutlet UILabel *lblSwiftPoints;

@end
