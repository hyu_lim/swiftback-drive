
#import "BaseVC.h"


@interface ContactVC : BaseVC <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webViewContact;
- (IBAction)backBtnPressed:(id)sender;
@property (strong , nonatomic) NSDictionary *dictContact;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

@end
