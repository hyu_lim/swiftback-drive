//
//  AboutTutorialTableViewController.h
//  SwiftBack Driver
//
//  Created by Zin Mar Htet on 13/2/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollTutorialViewController.h"

@interface AboutTutorialTableViewController : UITableViewController

@end
