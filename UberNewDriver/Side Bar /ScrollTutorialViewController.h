//
//  ScrollTutorialViewController.h
//  SwiftBack Driver
//
//  Created by Zin Mar Htet on 13/2/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AboutTutorialTableViewController.h"

@interface ScrollTutorialViewController : UIViewController

@property NSArray *pageLabel;
@property NSArray *pageImages;

@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UIImageView *tutorialImg;

@end
