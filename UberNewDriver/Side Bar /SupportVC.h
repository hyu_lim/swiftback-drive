

#import "BaseVC.h"
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface SupportVC : BaseVC<MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
}



@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *blurView;

- (IBAction)btnEmailClick:(id)sender;
- (IBAction)btnCallClick:(id)sender;

@end
