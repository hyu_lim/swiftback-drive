
#import "BaseVC.h"
#import "Social/Social.h"
#import <MessageUI/MessageUI.h>

@interface ShareVC : BaseVC<MFMailComposeViewControllerDelegate,UINavigationControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
}


@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnWhatsapp;
@property (weak, nonatomic) IBOutlet UIButton *btnMail;
@property (weak, nonatomic) IBOutlet UIView *blurView;

- (IBAction)onClickFacebook:(id)sender;
- (IBAction)onClickTwitter:(id)sender;
- (IBAction)onClickMsg:(id)sender;
- (IBAction)onClickWhatsapp:(id)sender;

@end
