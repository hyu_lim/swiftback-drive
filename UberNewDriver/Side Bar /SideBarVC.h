
#import "BaseVC.h"
#import "SWRevealViewController.h"
#import "PickMeUpMapVC.h"

//@protocol timerProtocol <NSObject>
//
//-(void)invalidateTimer;
//
//@end

@interface SideBarVC : BaseVC <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    BOOL internet;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) PickMeUpMapVC *ViewObj;
@property (nonatomic, weak) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
//@property (strong,nonatomic) id <timerProtocol> delegate;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@end
