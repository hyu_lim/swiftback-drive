//
//  LeaderboardVC.m
//  SwiftBack Driver
//
//  Created by Elluminati on 12/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "LeaderboardVC.h"
#import "LeaderBoardCell.h"
#import "UIView+Utils.h"
#import "UIImageView+Download.h"
#import "SWRevealViewController.h"

@interface LeaderboardVC ()

@end

@implementation LeaderboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrSwiftPoints = [[NSArray alloc]init];
    [self getLeaderPoints];
    [self customSetup];
    [self layoutSetup];
    
    //UITapGestureRecognizer setup
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideUpAndDownWithTapGestureRecognizer:)];
    
    [self.viewForExplanation addGestureRecognizer:singleTapGestureRecognizer];
    
    //UITapGestureRecognizer setup
    UITapGestureRecognizer *singleTapGestureRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePopUpWithTapGestureRecognizer:)];
    
    [self.blurView addGestureRecognizer:singleTapGestureRecognizer2];
}

- (void)layoutSetup {
    
    //blurView
    _blurViewForCurrentPosition.layer.cornerRadius = 10;
    _blurViewForCurrentPosition.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurViewForCurrentPosition.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurViewForCurrentPosition.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurViewForCurrentPosition addSubview:blurEffectView];
        
    } else {
        self.blurViewForCurrentPosition.backgroundColor = [UIColor blackColor];
    }
    
    //viewForCurrentPosition
    _viewForCurrentPosition.layer.cornerRadius = 10;
    
    //backgroundImgView
    _backgroundImgView.layer.cornerRadius = 10;
    
    //blurViewForExplanation
    _blurViewForExplanation.layer.cornerRadius = 10;
    _blurViewForExplanation.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurViewForExplanation.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurViewForExplanation.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurViewForExplanation addSubview:blurEffectView];
        
    } else {
        self.blurViewForExplanation.backgroundColor = [UIColor blackColor];
    }
    
    //blurView
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    
    //viewForExplanation
    _viewForExplanation.layer.cornerRadius = 10;
    
    //backgroundImgForExplanation
    _backgroundImgForExplanation.layer.cornerRadius = 10;
    
    //viewForInformation
    _viewForInformation.layer.cornerRadius = 10;
    
    //lblCircle1
    _lblCircle1.layer.cornerRadius = 40;
    _lblCircle1.layer.borderColor = [UIColor whiteColor].CGColor;
    _lblCircle1.layer.borderWidth = 1;
    
    //lblCircle2
    _lblCircle2.layer.cornerRadius = 40;
    _lblCircle2.layer.borderColor = [UIColor whiteColor].CGColor;
    _lblCircle2.layer.borderWidth = 1;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark Reveal Setup

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark
#pragma mark - Get Leader points

-(void)getLeaderPoints{
    
    if ([[AppDelegate sharedAppDelegate] connected]) {
        
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        
        [APPDELEGATE showLoadingWithTitle:@"Loading"];
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_LEADER_POINTS withParamData:dictParam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response) {
    
                if ([[response valueForKey:@"success"]boolValue]) {
                    
                    arrSwiftPoints = [response objectForKey:@"leaders"];
                    
                    if ([arrSwiftPoints count] > 0) {
                        [tableForBoard reloadData];
                        [tableForBoard setHidden:NO];
                        
                        [self.profileImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
                        [self.profileImgView downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC3"]];
                        
                        
                        [self.lblWalkerName setText:[NSString stringWithFormat:@"%@",[arrUser valueForKey:@"first_name"]]];
                        [self.lblWalkerPoints setText:[NSString stringWithFormat:@"%.f pt",[[response valueForKey:@"my_swift_points"] doubleValue]]];
                        [self.lblWalkerRank setText:[NSString stringWithFormat:@"#%@",[response valueForKey:@"my_rank"]]];
                        
                        
                    }
                    else{
                        [tableForBoard setHidden:YES];
                    }
                    
                }
                else{
                    [tableForBoard setHidden:YES];

                }
            }
        }];
    }
}

#pragma mark
#pragma mark Tableview Data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [arrSwiftPoints count];
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LeaderBoardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BoardCell"];
    
    NSDictionary *dictWalker = [arrSwiftPoints objectAtIndex:indexPath.row];
    
    [cell.driverProPic applyRoundedCornersFullWithColor:[UIColor colorWithRed:27.0f/255.f green:156.0f/255.0f blue:120.0f/255.f alpha:1.0f]];
    
    [cell.driverProPic downloadFromURL:[dictWalker objectForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC3"]];
    
    
    [cell.lblDriverName setText:[NSString stringWithFormat:@"%@. %@", [dictWalker objectForKey:@"rank"] ,[dictWalker objectForKey:@"walker_name"] ]];
    
    [cell.lblSwiftPoints setText:[NSString stringWithFormat:@"%.f",[[dictWalker objectForKey:@"swift_points"] doubleValue]]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 120.f;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10.0f;
}

#pragma mark
#pragma mark Animation

- (IBAction)onClickRewardBtn:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://swiftback.com/leaderboards/"]];
}

- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer {
    
    CGFloat newYCoordinate = 530.0;
    
    if(self.viewForExplanation.frame.origin.y == 530.0) {
        newYCoordinate = 320.0;
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        
        self.viewForExplanation.frame = CGRectMake(self.viewForExplanation.frame.origin.x, newYCoordinate, self.viewForExplanation.frame.size.width, self.viewForExplanation.frame.size.height);
        
        self.blurViewForExplanation.frame = CGRectMake(self.blurViewForExplanation.frame.origin.x, newYCoordinate, self.blurViewForExplanation.frame.size.width, self.blurViewForExplanation.frame.size.height);
    }];
}

- (void)hidePopUpWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer {
    
    [_viewForInformation setHidden: YES];
    [_blurView setHidden: YES];
}

- (IBAction)onClickInfoBtn:(id)sender {
    
    [_viewForInformation setHidden: NO];
    [_blurView setHidden: NO];
}


@end
