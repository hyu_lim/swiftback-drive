
#import "SupportVC.h"
#import "BaseVC.h"
#import "SWRevealViewController.h"

@interface SupportVC ()

@end

@implementation SupportVC

- (void)viewDidLoad {
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    
    [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle: ) forControlEvents:UIControlEventTouchUpInside];
    [self.viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    // Do any additional setup after loading the view.
    
    //Support button
    _btnEmail.layer.cornerRadius = 5;
    _btnEmail.layer.borderWidth = 1;
    _btnEmail.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //rectangle background Rect
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    //blurView
    _blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnEmailClick:(id)sender {
    
    
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:NSLocalizedString(@"SWIFTBACK_SUPPORT_MAIL", nil)];
    [mailComposer setToRecipients:[[NSArray alloc] initWithObjects:@"support@swiftback.com", nil]];
    [mailComposer setMessageBody:[NSString stringWithFormat:@"\n\n\nReference\nName:%@ %@\nUser ID:%@\nApp Version: %@\n", [arrUser valueForKey:@"first_name"], [arrUser valueForKey:@"last_name"],[arrUser valueForKey:@"id"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]] isHTML:NO];
    
    [self presentViewController:mailComposer animated:YES completion:nil];
    
}
#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)btnCallClick:(id)sender {
    
    NSString *strProviderPhone=@"+6590999888";
    
    NSString *call=[NSString stringWithFormat:@"tel://%@",strProviderPhone];
    
    if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:call]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"ALERT", nil) message:NSLocalizedString(@"CALL_FACILITY_IS_NOT_AVAILABLE", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
@end
