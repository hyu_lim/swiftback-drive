

#import "BaseVC.h"
#import "RatingBar.h"

@interface ProfileVC : BaseVC <UITextFieldDelegate,UIScrollViewDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtTaxiNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtCarNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollProfile;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtZip;
@property (weak, nonatomic) IBOutlet UITextField *txtBio;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIImageView *profileBackground;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtReNewPassword;
@property (weak, nonatomic) IBOutlet UIImageView *bgPwd;
@property (weak, nonatomic) IBOutlet UIImageView *bgNewPwd;
@property (weak, nonatomic) IBOutlet UIImageView *bgReNewPwd;
@property (weak, nonatomic) IBOutlet UITextField *txtTaxiModel;
@property (weak, nonatomic) IBOutlet UITextField *txtCarColor;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnLogOut;
@property (weak, nonatomic) IBOutlet UIButton *btnCountryCode;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UIButton *btnProPic;
@property (weak, nonatomic) IBOutlet UIImageView *imgCarModelInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblCarModelInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imgCarNumberInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblCarNumberInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmailInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailInfo;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;
@property (weak, nonatomic) IBOutlet UIButton *btnInfo1;
@property (weak, nonatomic) IBOutlet UIButton *btnInfo2;
@property (weak, nonatomic) IBOutlet UIButton *btnInfo3;
@property (weak, nonatomic) IBOutlet UIView *viewResetPwd;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundReset;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundVehicleInfo;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIView *viewForEarnings;
@property (weak, nonatomic) IBOutlet UILabel *lblSavingOfMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblGrossSavings;
@property (weak, nonatomic) IBOutlet UIView *viewForMonthlyGoal;
@property (weak, nonatomic) IBOutlet UITextField *txtMontlyGoal;
@property (weak, nonatomic) IBOutlet UIProgressView *pvSaving;
@property (weak, nonatomic) IBOutlet UIView *viewForGreenOverlay;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthlyGoal;
@property (weak, nonatomic) IBOutlet UIButton *documentBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnResetPassword;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *bgNavBarImgView;

- (IBAction)LogOutBtnPressed:(id)sender;
- (IBAction)editBtnPressed:(id)sender;
- (IBAction)updateBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)imgPicBtnPressed:(id)sender;
- (IBAction)onClickEmailInfo:(id)sender;
- (IBAction)onClickTaxiModelInfo:(id)sender;
- (IBAction)onClickTaxiNoInfo:(id)sender;
- (IBAction)onClickResetPwd:(id)sender;
- (IBAction)onClickCancelPwd:(id)sender;
- (IBAction)onClickReset:(id)sender;
- (IBAction)saveMonthlyGoal:(id)sender;
- (IBAction)setMonthlyGoal:(id)sender;
- (IBAction)cancelMonthlyGoal:(id)sender;
- (IBAction)onClickPresentModal:(id)sender;

- (void)showCurrentSavingWithPV;
- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer;



@end
