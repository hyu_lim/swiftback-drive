//
//  CreateOfferVC.h
//  SwiftBack Driver
//
//  Created by Elluminati on 18/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

@protocol CreateOffer <NSObject>

-(void)rideOfferCreated;

@end

@interface CreateOfferVC : UIViewController<UITextFieldDelegate,GMSMapViewDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate>{
    
    
    NSMutableArray *placeMarkArr;
    NSArray *routes;
    
    CLLocationManager *locationManager;
    
    GMSMarker *driverMarker;
    GMSMarker *sourceMarker,*destinationMarker;
    GMSMapView *mapView_;
    
    CLLocationCoordinate2D sourcecoordinate,destinationcoordinate;
    
    NSString *isSource,*placeID;
    NSString *sourceLatitude,*sourceLongitude;
    NSString *destinationLatitude,*destinationLongitude;
    NSString *distance,*cost;
    NSString *strFutureDate;
    BOOL isList;
    
    __weak IBOutlet UIButton *menuBtn;
    
   //Address Outlets
    __weak IBOutlet UIView *viewForAddress;
    
    __weak IBOutlet UITextField *txtSource;
    __weak IBOutlet UITextField *txtDestination;
    
    __weak IBOutlet UITableView *tableForPlaces;
    
    __weak IBOutlet UIView *viewForGoogleMap;
    
    // Estimate Outlets
    __weak IBOutlet UIView *viewForEstimate;
    
    __weak IBOutlet UIView *viewForOfferInfo;
    
    __weak IBOutlet UIButton *estimateNextBtn;
    
    __weak IBOutlet UILabel *lblEstimateDistance;
    
    __weak IBOutlet UILabel *lblOfferPrice;
    
    __weak IBOutlet UIButton *minusBtn;
    
    __weak IBOutlet UIButton *plusBtn;
    
    // Date Outlets
    __weak IBOutlet UIView *viewForDate;
    
    __weak IBOutlet UIView *viewForDatePicker;
    
    __weak IBOutlet UIButton *dateNextBtn;
    
    __weak IBOutlet UIDatePicker *datepicker;
    
    // Note Outlets
    __weak IBOutlet UIView *viewForNotes;
    
    __weak IBOutlet UIView *viewForNoOfPassenger;
    
    __weak IBOutlet UILabel *lblPassengerCount;
    
    __weak IBOutlet UITextField *txtNotes;
    
	__weak IBOutlet UIButton *noteNextBtn;
    
    __weak IBOutlet UIStepper *passengerStepper;
    
    
    //Greenoverlay view
    __weak IBOutlet UIView *viewForGreenOverlay;
    
    // Confirm offer Outlets
    __weak IBOutlet UIView *viewForConfirm;
    
    __weak IBOutlet UILabel *lblFrom;
    
    __weak IBOutlet UILabel *lblTo;
    
    __weak IBOutlet UILabel *lblDate;
    
    __weak IBOutlet UILabel *lblTime;
    
    __weak IBOutlet UILabel *lblDistance;
    
    __weak IBOutlet UILabel *lblNotes;
    
    __weak IBOutlet UILabel *lblSeats;
    
    __weak IBOutlet UILabel *lblPrice;
    
    __weak IBOutlet UIButton *confirmOfferBtn;
    
    
}

@property (weak, nonatomic) IBOutlet UIView *blurViewForAddress;

@property (strong,nonatomic) id <CreateOffer> delegate;

- (IBAction)onClickBack:(id)sender;

// Address Events
-(IBAction)onClickSearch:(UITextField*)sender;

// Estimate View events
- (IBAction)onClickEstimateNext:(id)sender;
- (IBAction)onClickMinus:(id)sender;
- (IBAction)onClickPlus:(id)sender;


// Date View Events
- (IBAction)onClickDateNext:(id)sender;
- (IBAction)goBackToEstimate:(id)sender;

// Note View Events
- (IBAction)onClickPassengerStepper:(id)sender;
- (IBAction)onClickNotesNext:(id)sender;

- (IBAction)goBackToDateView:(id)sender;

// Confirm Offer Events
- (IBAction)onClickEditOffer:(id)sender;
- (IBAction)onClickConfirmOffer:(id)sender;









@end
