    
#import "RideNextVC.h"
#import "RideNextCell.h"
#import "RideNowCell.h"
#import "SWRevealViewController.h"
#import "Constant.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import <MapKit/MapKit.h>
#import "AdvancePickupVC.h"
#import "RatingBar.h"


@interface RideNextVC (){
    
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableString *strRequsetId;
}

@end

@implementation RideNextVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self customSetup];
    [self layoutSetup];
}

-(void)layoutSetup {
    
    [noItemsImgView setHidden:NO];
    
    //Segmented button properties
    CGRect frame= segmentToggle.frame;
    [segmentToggle setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 25)];
    
    _confirmRidesBtn.layer.cornerRadius = 7;
    _confirmRidesBtn.layer.borderWidth = 1;
    _confirmRidesBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //  counterTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(counter) userInfo:nil repeats:YES];
    
    //NYSegmentedControl
    
    self.customSegmentedControl = [[NYSegmentedControl alloc] initWithItems:@[@"Nearest", @"Earliest"]];
    self.customSegmentedControl.frame = CGRectMake(10, 60, 300, 35);
    [self.customSegmentedControl addTarget:self action:@selector(segmentSelected) forControlEvents:UIControlEventValueChanged];
    
    self.customSegmentedControl.titleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleFont = [UIFont systemFontOfSize:12.0f];
    
    self.customSegmentedControl.backgroundColor = [UIColor colorWithRed:37.f/255.f green:131.f/255.f blue:111.f/255.f alpha:1.0f];

    self.customSegmentedControl.borderWidth = 0.0f;
    
    self.customSegmentedControl.segmentIndicatorBackgroundColor = [UIColor colorWithRed:59.f/255.f green:177.f/255.f blue:156.f/255.f alpha:1.0f];
    self.customSegmentedControl.segmentIndicatorBorderWidth = 0.0f;
    self.customSegmentedControl.segmentIndicatorInset = 2.0f;
    self.customSegmentedControl.segmentIndicatorBorderColor = self.view.backgroundColor;
    
    self.customSegmentedControl.cornerRadius = CGRectGetHeight(self.customSegmentedControl.frame) / 2.0f;
    
    [self.view addSubview:self.customSegmentedControl];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
//    if ([segmentToggle selectedSegmentIndex] ==0) {
//        [self getRequests:@"nearest"];
//
//    }
//    else{
//        [self getRequests:@"earliest"];
//    }
    
    if ([self.customSegmentedControl selectedSegmentIndex] ==0) {
        [self getRequests:@"nearest"];
        
    }
    else{
        [self getRequests:@"earliest"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//-(void)counter
//{
//    NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
//    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
//    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
//    
//    AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
//    [helper getDataFromPath:FILE_REQUEST_COUNTER withParamData:dictParam withBlock:^(id response, NSError *error) {
//        if (response)
//        {
//            if ([[response objectForKey:@"success"] boolValue])
//            {
//                if ([[response objectForKey:@"count"] integerValue ]== 0)
//                {
//                    //[self.confirmRidesBtn setHidden:YES];
//                }
//                else
//                {
//                    [self.confirmRidesBtn setHidden:NO];
//                    [self.confirmRidesBtn setTitle:[NSString stringWithFormat:@"%ld",(long)[[response objectForKey:@"count"] integerValue]] forState:UIControlStateNormal];
//                    [self.confirmRidesBtn setBackgroundColor:[UIColor redColor]];
//                    [self.confirmRidesBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                }
//            }
//        }
//    }];
//    
//    
//}

#pragma mark
#pragma mark Reveal Setup

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark
#pragma mark - Get Future reqeusts

-(void)getRequests:(NSString*)type
{
    if ([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:@"1" forKey:type];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_ALL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response)
            {
                if ([[response objectForKey:@"success"] boolValue])
                {
                    if ([type isEqualToString:@"nearest"]) {
                        nearestRequest = [[response valueForKey:@"request"] mutableCopy];
                        
                    }
                    else{
                        earliestRequest = [[response valueForKey:@"request"] mutableCopy];
                    }
                    
                    if ([type isEqualToString:@"nearest"] && [nearestRequest count] > 0)
                    {
                        
                        [tblRequestList reloadData];
                        [tblRequestList setHidden:NO];
                        [noItemsImgView setHidden:YES];
                    }
                    else if ([type isEqualToString:@"earliest"] && [earliestRequest count] >0){
                        
                        [tblRequestList reloadData];
                        [tblRequestList setHidden:NO];
                        [noItemsImgView setHidden:YES];
                        
                    }
                    else
                    {
                        [tblRequestList setHidden:YES];
                        [noItemsImgView setHidden:NO];
                        
                    }
                    
                }
                else{
                    
                    [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                    [tblRequestList setHidden:YES];
                    [noItemsImgView setHidden:NO];
                }
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
    
    
    //    else
    //    {
    //        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Swiftback Drive -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //        alertLocation.tag=100;
    //        [alertLocation show];
    //    }
    
}

#pragma mark
#pragma mark - TableView Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        return [nearestRequest count];
        
    }
    else if (self.customSegmentedControl.selectedSegmentIndex == 1){
        
        return [earliestRequest count];
    }
    else
    {
        return [confirmRequests count];
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


    NSDictionary *dictRequest;
    
    RideNowCell *cellZero = [tableView dequeueReusableCellWithIdentifier:@"CellZero"];
    RideNextCell *cellOne = [tableView dequeueReusableCellWithIdentifier:@"CellOne"];

    
//    if (segmentToggle.selectedSegmentIndex == 0) {
//         dictRequest = [nearestRequest objectAtIndex:indexPath.row];
//    }
//    else if (segmentToggle.selectedSegmentIndex == 1){
//         dictRequest = [earliestRequest objectAtIndex:indexPath.row];
//    }
//    else{
//        dictRequest = [confirmRequests objectAtIndex:indexPath.row];
//    }
    
    if (self.customSegmentedControl.selectedSegmentIndex == 0) {
        dictRequest = [nearestRequest objectAtIndex:indexPath.row];
    }
    else if (self.customSegmentedControl.selectedSegmentIndex == 1){
        dictRequest = [earliestRequest objectAtIndex:indexPath.row];
    }
    else{
        dictRequest = [confirmRequests objectAtIndex:indexPath.row];
    }

    
    NSDictionary *dictOwner = [dictRequest  valueForKey:@"owner"];

    
    if ([[dictRequest objectForKey:@"later"]integerValue] == 0) {
     
        //Ride Now Properties
//        cellZero.layer.cornerRadius = 10;
//        cellZero.layer.shadowRadius = 5.0;
//        cellZero.layer.shadowOpacity = 0.4;
//        cellZero.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        cellZero.layer.borderWidth = 0.5;
//        cellZero.layer.opacity = 0.6;
//        cellZero.layer.backgroundColor = (__bridge CGColorRef)([UIColor colorWithRed:1 green:1 blue:1 alpha:1]);
//        cellZero.clipsToBounds = YES;
        
        UIView *viewForCellZero = [cellZero viewWithTag:903];
        viewForCellZero.layer.cornerRadius = 10;
        viewForCellZero.clipsToBounds = YES;

        
        //Adjust horizontal and vertical lines size
        UILabel *horizontalLine = [cellZero viewWithTag:906];
        UILabel *verticalLine = [cellZero viewWithTag:907];
        
        horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
        
        verticalLine.frame = CGRectMake(verticalLine.frame.origin.x, verticalLine.frame.origin.y, 0.5, verticalLine.frame.size.height);

        if ([[dictRequest objectForKey:@"payment_mode"]boolValue]) {
            // Cash Image
            [cellZero.paymentImgView setImage:[UIImage imageNamed:@"icon_cash-01"]];
        }
        else{
            [cellZero.paymentImgView setImage:[UIImage imageNamed:@"icon_card-01"]];
        }

        
        [cellZero.lblFromStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"s_address"]]];
        [cellZero.lblToStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"d_address"]]];
        [cellZero.lblCost setText:[NSString stringWithFormat:@"$%@",[dictRequest valueForKey:@"total"]]];
        [cellZero.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
        
        [cellZero.lblPassengerName setText:[dictOwner valueForKey:@"name"]];
        
        [cellZero.profileImgView  applyRoundedCornersFullWithColor:[UIColor clearColor]];
        [cellZero.profileImgView downloadFromURL:[dictOwner objectForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
        
        
//        if([[dictOwner objectForKey:@"picture"] isEqualToString:@""]) {
//            NSLog(@"NO PROFILE");
//            [cellZero.profileImgView setHidden: YES];
//            [cellZero.lblPassengerName setFrame:CGRectMake(cellZero.lblPassengerName.frame.origin.x, 30 , cellZero.lblPassengerName.frame.size.width, cellZero.lblPassengerName.frame.size.height)];
//            [cellZero.ratingView setFrame:CGRectMake(cellZero.ratingView.frame.origin.x, 50 , cellZero.ratingView.frame.size.width, cellZero.ratingView.frame.size.height)];
//        } else {
//            [cellZero.profileImgView setHidden: NO];
//            [cellZero.lblPassengerName setFrame:CGRectMake(cellZero.lblPassengerName.frame.origin.x, 67, cellZero.lblPassengerName.frame.size.width, cellZero.lblPassengerName.frame.size.height)];
//            [cellZero.ratingView setFrame:CGRectMake(cellZero.ratingView.frame.origin.x, 86, cellZero.ratingView.frame.size.width, cellOne.ratingView.frame.size.height)];
//            [cellZero.profileImgView downloadFromURL:[dictOwner objectForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
//        }


        
        [cellZero.ratingView setUserInteractionEnabled:NO];
        
        RBRatings rate=([[dictOwner valueForKey:@"rating"]floatValue]*2);
        [cellZero.ratingView setRatings:rate];
        
        return cellZero;
    }
    else{
        //Ride Next Properties
//        cellOne.layer.cornerRadius = 10;
//        cellOne.layer.shadowRadius = 5.0;
//        cellOne.layer.shadowOpacity = 0.4;
//        cellOne.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        cellOne.layer.borderWidth = 0.5;
//        cellOne.layer.opacity = 0.6;
//        cellOne.layer.backgroundColor = (__bridge CGColorRef)([UIColor colorWithRed:1 green:1 blue:1 alpha:1]);
//        cellOne.clipsToBounds = YES;
        
        UIView *viewForCellOne = [cellOne viewWithTag:904];
        viewForCellOne.layer.cornerRadius = 10;
        viewForCellOne.clipsToBounds = YES;
        
        //Adjust horizontal and vertical lines size
        UILabel *horizontalLine = [cellOne viewWithTag:908];
        UILabel *verticalLine1 = [cellOne viewWithTag:909];
        UILabel *verticalLine2 = [cellOne viewWithTag:910];
        
        horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
        
        verticalLine1.frame = CGRectMake(verticalLine1.frame.origin.x, verticalLine1.frame.origin.y, 0.5, verticalLine1.frame.size.height);
        
        verticalLine2.frame = CGRectMake(verticalLine2.frame.origin.x, verticalLine2.frame.origin.y, 0.5, verticalLine2.frame.size.height);

        if ([[dictRequest objectForKey:@"payment_mode"]boolValue]) {
            // Cash Image
            [cellOne.paymentImgView setImage:[UIImage imageNamed:@"icon_cash-01"]];
        }
        else{
            [cellOne.paymentImgView setImage:[UIImage imageNamed:@"icon_card-01"]];
        }
        
        [cellOne.lblFromStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"s_address"]]];
        [cellOne.lblToStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"d_address"]]];
        [cellOne.lblCost setText:[NSString stringWithFormat:@"$%@",[dictRequest valueForKey:@"total"]]];
        [cellOne.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
        
        NSDate *date = [[UtilityClass sharedObject] stringToDate:[dictRequest valueForKey:@"future_request_date"] withFormate:@"yyyy-MM-dd"];


        [cellOne.lblDepartureDate setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMM"]];
        
        [cellOne.lblDepartureTime setText:[dictRequest valueForKey:@"future_request_time"]];
        
        [cellOne.lblName setText:[dictOwner valueForKey:@"name"]];
        
        [cellOne.profileImgView  applyRoundedCornersFullWithColor:[UIColor clearColor]];
        [cellOne.profileImgView downloadFromURL:[dictOwner objectForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
        
//        if([[dictOwner objectForKey:@"picture"] isEqualToString:@""]) {
//            NSLog(@"NO PROFILE");
//            [cellOne.profileImgView setHidden: YES];
//            [cellOne.lblName setFrame:CGRectMake(cellOne.lblName.frame.origin.x, 30 , cellOne.lblName.frame.size.width, cellOne.lblName.frame.size.height)];
//            [cellOne.ratingView setFrame:CGRectMake(cellOne.ratingView.frame.origin.x, 50 , cellOne.ratingView.frame.size.width, cellOne.ratingView.frame.size.height)];
//        } else {
//            [cellOne.profileImgView setHidden: NO];
//            [cellOne.lblName setFrame:CGRectMake(cellOne.lblName.frame.origin.x, 67, cellOne.lblName.frame.size.width, cellOne.lblName.frame.size.height)];
//            [cellOne.ratingView setFrame:CGRectMake(cellOne.ratingView.frame.origin.x, 86, cellOne.ratingView.frame.size.width, cellOne.ratingView.frame.size.height)];
//            [cellOne.profileImgView downloadFromURL:[dictOwner objectForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
//        }
        
        [cellOne.ratingView setUserInteractionEnabled:NO];
        
        RBRatings ratings = (float)([[dictOwner objectForKey:@"rating"] floatValue]*2);
        [cellOne.ratingView setRatings:ratings];
        
        return cellOne;

    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return 160.0f;
    return 105.0f;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

#pragma mark
#pragma mark - Tableview Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictRequest;
//    if ([segmentToggle selectedSegmentIndex] == 0) {
//        
//        dictRequest = [nearestRequest objectAtIndex:indexPath.row];
//    }
//    else if ([segmentToggle selectedSegmentIndex] == 1){
//        
//        dictRequest = [earliestRequest objectAtIndex:indexPath.row];
//    }
//    else{
//        dictRequest = [confirmRequests objectAtIndex:indexPath.row];
//    }
    
    if ([self.customSegmentedControl selectedSegmentIndex] == 0) {
        
        dictRequest = [nearestRequest objectAtIndex:indexPath.row];
    }
    else if ([self.customSegmentedControl selectedSegmentIndex] == 1){
        
        dictRequest = [earliestRequest objectAtIndex:indexPath.row];
    }
    else{
        dictRequest = [confirmRequests objectAtIndex:indexPath.row];
    }
	
	[self performSegueWithIdentifier:SEGUE_TO_ADVANCE_PICKUP sender:dictRequest];
}

#pragma mark
#pragma mark Segue Method
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:SEGUE_TO_ADVANCE_PICKUP])
    {
        AdvancePickupVC *advance = [segue destinationViewController];
        advance.dictRequest = sender;
    }
}

- (void)segmentSelected {
    if ([self.customSegmentedControl selectedSegmentIndex]==0)
    {
        [self getRequests:@"nearest"];

    }
    else if([self.customSegmentedControl selectedSegmentIndex] == 1)
    {
        [self getRequests:@"earliest"];
        
    }
}


- (IBAction)onClickConfirmRides:(id)sender {
    
    [self performSegueWithIdentifier:SEGUE_TO_CONFIRMED_RIDES sender:nil];
}

@end
