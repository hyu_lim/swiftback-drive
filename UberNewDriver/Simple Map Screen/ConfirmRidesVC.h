//
//  ConfirmRidesVC.h
//  SwiftBack Driver
//
//  Created by Elluminati on 15/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmRidesVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *confirmRides;
    NSMutableArray *arrForDate;
    
    __weak IBOutlet UITableView *tableForConfirmRides;
    __weak IBOutlet UIImageView *noItemsImgView;
    
}
- (IBAction)onClickBack:(id)sender;
@end
