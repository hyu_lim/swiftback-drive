

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "NYSegmentedControl.h"

@interface RideNextVC : BaseVC <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    
    __weak IBOutlet UITableView *tblRequestList;
    
    __weak IBOutlet UIButton *menuBtn;
    
    __weak IBOutlet UISegmentedControl *segmentToggle;
    __weak IBOutlet UIImageView *noItemsImgView;
    __weak IBOutlet UIView *viewForNavigation;
    
    NSMutableArray *nearestRequest;
    NSMutableArray *earliestRequest;
    NSMutableArray *arrIndex;
    NSMutableArray *confirmRequests;
    NSInteger selectedCell;
    NSString *strType;
    NSDictionary *selectedRequest;
    
    NSTimer *counterTimer;
}


@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *confirmRidesBtn;
@property (nonatomic, strong) NYSegmentedControl *customSegmentedControl;

- (IBAction)onClickSegment:(id)sender;
- (IBAction)onClickConfirmRides:(id)sender;

@end
