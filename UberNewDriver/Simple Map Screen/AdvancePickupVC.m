


#import "AdvancePickupVC.h"
#import "ChatVC.h"

@interface AdvancePickupVC ()

@end

@implementation AdvancePickupVC
@synthesize dictRequest;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self customSetup];
    [self layoutSetup];
    [self setData];
    
    [APPDELEGATE showLoadingWithTitle:@"Loading"];
    
    //SwipeGestureRecognizer setup
    
    UISwipeGestureRecognizer *swipeUpBackgroundRect = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideUpWithGestureRecognizer:)];
    swipeUpBackgroundRect.direction = UISwipeGestureRecognizerDirectionUp;
    
    UISwipeGestureRecognizer *swipeDownBackgroundRect = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideDownWithGestureRecognizer:)];
    swipeDownBackgroundRect.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.frontView addGestureRecognizer:swipeUpBackgroundRect];
    [self.frontView addGestureRecognizer:swipeDownBackgroundRect];
    
    //UITapGestureRecognizer setup
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideUpAndDownWithTapGestureRecognizer:)];
    
    [self.frontView addGestureRecognizer:singleTapGestureRecognizer];
}

- (void)layoutSetup {
    
    [_UserImgView.layer setCornerRadius:_UserImgView.frame.size.width / 2];
    [_UserImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    
    [self.ratingView initRateBar];
    
    //frontView
    _frontView.layer.cornerRadius = 10;
    
    //roundedRectView
    _roundedRectView.layer.cornerRadius = 10;
    _roundedRectView.layer.shadowRadius = 5.0;
    _roundedRectView.layer.shadowOpacity = 0.4;
    
    //lblBtnsBackground
    _lblBtnsBackground.layer.cornerRadius = 10;
    _lblBtnsBackground.clipsToBounds = YES;
    
    //ChatBtm
    _chatBtn.layer.cornerRadius = 5;
    
    //    [_lblNote sizeToFit];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}



-(void)loginForQuickBlox{
    
    // NSDictionary *dictLogin = [USERDEFAULT objectForKey:PREF_LOGIN_OBJECT];
    
    NSString *fullname = [NSString stringWithFormat:@"%@%@",[arrUser valueForKey:PARAM_FIRST_NAME],[arrUser valueForKey:PARAM_LAST_NAME]];
    // QBUUser *currentUser = [QBUUser user];
    //  [currentUser setPassword:[fullname stringByAppendingFormat:@"%@",[dictLogin objectForKey:PARAM_ID]]];
    //  [currentUser setLogin:fullname];
    
    if (currentUser == nil) {
        currentUser = [QBUUser user];
        // [currentUser setLogin:fullname];
        [currentUser setEmail:[arrUser valueForKey:PARAM_EMAIL]];
        //  fullname = [fullname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        fullname = [fullname stringByAppendingFormat:@"%@",[arrUser valueForKey:PARAM_ID]];
        [currentUser setPassword:fullname];
        
        
    }
    [[QMServicesManager instance] logInWithUser:currentUser completion:^(BOOL success, NSString *errorMessage) {
        if (success) {
            
            // connect to Chat
            [[QBChat instance] connectWithUser:currentUser completion:^(NSError * _Nullable error) {
                
                NSString *stringCheck = [error.userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
                if ([stringCheck isEqualToString:@"You are already connected to chat."] || !error) {
                    
                    isQuickbloxConnected = YES;
                    [self getDriverInfo];
                    
                }
                
            }];
            
        }
        else{
            [self getDriverInfo];
            
        }
    }];
    
}

-(void)setData
{
    if ([[self.dictRequest objectForKey:@"later"]integerValue] == 0) {
        
        [self.acceptBtn setHidden:NO];
        [self.rejectBtn setHidden:YES];
        //            [self.chatBtn setHidden:YES];
        
        
    }
    else{
        if ([[self.dictRequest objectForKey:@"status"] integerValue] == 0)
        {
            [self.acceptBtn setHidden:NO];
            [self.rejectBtn setHidden:YES];
            //            [self.chatBtn setHidden:YES];
        }
        else
        {
            [self.acceptBtn setHidden:YES];
            [self.rejectBtn setHidden:NO];
            [self loginForQuickBlox];
        }
        
    }
    
    [self.UserImgView applyRoundedCornersFull];
    [self.scrollView setContentSize:CGSizeMake(0, 350)];
    
    NSDictionary *dictOwner = [self.dictRequest objectForKey:@"owner"];
    
    if ([[self.dictRequest objectForKey:@"no_of_passenger"] isKindOfClass:[NSNull class]] || ![self.dictRequest objectForKey:@"no_of_passenger"]) {
        
        [self.lblPassengerCount setText:@"N/A"];
    }
    else{
        
        [self.lblPassengerCount setText:[NSString stringWithFormat:@"%@",[self.dictRequest objectForKey:@"no_of_passenger"]]];
    }
    
    if([[self.dictRequest objectForKey:@"note"] isKindOfClass:[NSNull class]] || [[self.dictRequest objectForKey:@"note"] isEqualToString:@""] || ![self.dictRequest objectForKey:@"note"]){
        
        [self.lblNote setText:@"_"];
        
        [self.verticalLine3 setHidden:YES];
//        [self.lblNote_Label setHidden:YES];
//        [self.lblNote sonClicetHidden:YES];
        
    } else {
        
        [self.lblNote setText:[self.dictRequest objectForKey:@"note"]];
    }
    
    if ([[self.dictRequest objectForKey:@"payment_mode"]boolValue]) {
        
        // Cash Image
        [self.paymentImgView setImage:[UIImage imageNamed:@"icon_cash-01"]];
        [self.lblPayment setText:@"Cash :"];
        
    }
    else{
        
        [self.paymentImgView setImage:[UIImage imageNamed:@"icon_card-01"]];
        [self.lblPayment setText:@"Card :"];
    }

        [self.ratingView setUserInteractionEnabled:NO];
    //    [ratingView initRateBar];
    RBRatings ratings = (float)([[dictOwner valueForKey:@"rating"] floatValue] *2);
    
    [self.ratingView setRatings:ratings];

    
    //    NSString *payment_mode = [NSString stringWithFormat:@"%@",[self.dictRequest objectForKey:@"payment_mode"]];
    
    [self.UserImgView downloadFromURL:[dictOwner objectForKey:PARAM_PICTURE] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
    
    [self.lblName setText:[dictOwner objectForKey:@"name"]];
    [self.lblFrom setText:[self.dictRequest objectForKey:@"s_address"]];
    [self.lblTo setText:[self.dictRequest objectForKey:@"d_address"]];
    
    NSDate *date = [[UtilityClass sharedObject] stringToDate:[self.dictRequest valueForKey:@"future_request_date"] withFormate:@"yyyy-MM-dd"];
    
    [self.lblDate setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMM"]];
    [self.lblTime setText:[self.dictRequest valueForKey:@"future_request_time"]];
    [self.lblCost setText:[NSString stringWithFormat:@"$%@",[self.dictRequest valueForKey:@"total"]]];
    [self.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[self.dictRequest valueForKey:@"distance"] doubleValue]]];
    
    
    CLLocationCoordinate2D walker;
    walker.latitude=[[self.dictRequest objectForKey:@"walker_latitude"] doubleValue];
    walker.longitude=[[self.dictRequest objectForKey:@"walker_longitude"] doubleValue];
    
    CLLocationCoordinate2D source;
    source.latitude=[[self.dictRequest objectForKey:@"latitude"] doubleValue];
    source.longitude=[[self.dictRequest objectForKey:@"longitude"] doubleValue];
    
    CLLocationCoordinate2D destination;
    destination.latitude=[[self.dictRequest objectForKey:@"d_latitude"] doubleValue];
    destination.longitude=[[self.dictRequest objectForKey:@"d_longitude"] doubleValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:walker.latitude
                                                            longitude:walker.longitude
                                                                 zoom:15];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0,0,self.viewForGoogleMap.frame.size.width,self.viewForGoogleMap.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = YES;
    [self.viewForGoogleMap addSubview:mapView_];
    
    
    GMSMarker *driver = [[GMSMarker alloc]init];
    driver.position = walker;
    driver.icon = [UIImage imageNamed:@"pin_driver"];
    //    driver.icon = [self image:driver.icon scaledToSize:CGSizeMake(45.0f,45.0f)];
    driver.map = mapView_;
    
    GMSMarker *pickup = [[GMSMarker alloc]init];
    pickup.position = source;
    pickup.icon = [UIImage imageNamed:@"pin_client_org"];
    //    pickup.icon = [self image:pickup.icon scaledToSize:CGSizeMake(40.0f,40.0f)];
    pickup.map = mapView_;
    
    GMSMarker *to = [[GMSMarker alloc]init];
    to.position = destination;
    to.icon = [UIImage imageNamed:@"pin_client_destination"];
    //    to.icon = [self image:to.icon scaledToSize:CGSizeMake(45.0f,45.0f)];
    to.map = mapView_;
    
    UIColor *themeColor = [UIColor colorWithRed:51.0/255.0 green:204.0/255.0 blue:153.0/255.0 alpha:1.0];
    UIColor *blueColor = [UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSArray *decodeLine1 =  [self calculateRoutesFrom:walker t:source :themeColor];
        NSArray *decodeLine2 =  [self calculateRoutesFrom:source t:destination :blueColor];
        
        NSMutableArray *decodeLines = [[NSMutableArray alloc]init];
        [decodeLines addObjectsFromArray:decodeLine1];
        [decodeLines addObjectsFromArray:decodeLine2];
        
        [self centerMap:decodeLines];
        [self.viewForGoogleMap setUserInteractionEnabled:YES];
        [APPDELEGATE hideLoadingView];

    });
    
   
    
    //    For Polyline:
    //    (Driver current) - (Passenger "FROM") :  33cc99
    //    (Passenger "FROM") - (Passenger "TO") : 00CCFF
    
    //    [UIColor colorWithRed:51.0/255.0 green:204.0/255.0 blue:153.0/255.0 alpha:1.0];
    //        [UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:255.0/255.0 alpha:1.0];
    
}

-(void)getDriverInfo{
    
    [QBRequest userWithEmail:[[self.dictRequest objectForKey:@"owner"] objectForKey:@"email"] successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
        NSLog(@"%@",user);
        
        receipent = user;
        //        [self.chatBtn setHidden:NO];
        
    } errorBlock:^(QBResponse * _Nonnull response) {
        
    }];
    
}


- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }
    
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    
    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return image
    return image;
}

#pragma mark -
#pragma mark - Draw Route Methods

- (NSMutableArray *)decodePolyLine: (NSMutableString *)encoded
{
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\" options:NSLiteralSearch range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len)
    {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        //printf("[%f,", [latitude doubleValue]);
        //printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:loc];
    }
    return array;
}

-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f   t:(CLLocationCoordinate2D) t :(UIColor*)color
{
    //    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    //    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    //
    //    NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
    //    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    //    //NSLog(@"api url: %@", apiUrl);
    //    NSError* error = nil;
    //    NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSASCIIStringEncoding error:&error];
    //    NSString *encodedPoints = [apiResponse stringByMatching:@"points:\\\"([^\\\"]*)\\\"" capture:1L];
    //    return [self decodePolyLine:[encodedPoints mutableCopy]];
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    // NSString* via = [NSString stringWithFormat:@"%f,%f",wayPoint.latitude,wayPoint.longitude];

    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,Google_Browser_key];
    //chk
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] ||  [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
    {
    }
    else
    {
        GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
        GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 5.0f;
        singleLine.strokeColor = color;
        singleLine.map = mapView_;
        
        routes = json[@"routes"];
        
        
    }
    
    NSString *points=[[[routes objectAtIndex:0] objectForKey:@"overview_polyline"] objectForKey:@"points"];
    return [self decodePolyLine:[points mutableCopy]];
}


-(void)centerMap:(NSArray*)locations
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocationCoordinate2D location;
    for (CLLocation *loc in locations)
    {
        location.latitude = loc.coordinate.latitude;
        location.longitude = loc.coordinate.longitude;
        // Creates a marker in the center of the map.
        bounds = [bounds includingCoordinate:location];
    }
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:10.0f]];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    ChatVC *chatVC = [segue destinationViewController];
    chatVC.type = 1;
    chatVC.receipent = receipent;
    
}


- (IBAction)onClickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickAccept:(id)sender {
    
    
    if([APPDELEGATE  connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Loading..."];
        
        
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:[self.dictRequest objectForKey:@"request_id"] forKey:PARAM_REQUEST_ID];
        [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictparam setObject:@"1" forKey:PARAM_ACCEPTED];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_RESPOND_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             
             NSLog(@"Respond to Request= %@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Ride request confirmed.", nil) message:NSLocalizedString(@"you will be reminded of your pickup 20 minutes before.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
                 else{
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (IBAction)onClickReject:(id)sender {
    
    UIAlertController *cancelAlert = [UIAlertController alertControllerWithTitle:@"Cancel Request" message:@"Are you sure to want to cancel request?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        if([APPDELEGATE  connected])
        {
            [APPDELEGATE showLoadingWithTitle:@"Loading..."];
            NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
            
            [dictparam setObject:[self.dictRequest objectForKey:@"request_id"] forKey:PARAM_REQUEST_ID];
            [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_FUTURE_WALK withParamData:dictparam withBlock:^(id response, NSError *error)
             {
                 
                 NSLog(@"Cancel Request= %@",response);
                 [APPDELEGATE hideLoadingView];
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         
                         [self.navigationController popViewControllerAnimated:YES];
                     }
                     else{
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
                 
             }];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [cancelAlert  addAction:okAction];
    [cancelAlert addAction:cancelAction];
    
    [self presentViewController:cancelAlert animated:YES completion:nil];
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Ok"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)toMaps:(id)sender {
    NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%@,%@&daddr=%@,%@", [dictRequest valueForKey:@"latitude"], [dictRequest valueForKey:@"longitude"], [dictRequest valueForKey:@"d_latitude"], [dictRequest valueForKey:@"d_longitude"]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
}

#pragma mark - SwipeGestureRecognizer methods

- (void)slideUpWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer {
    
    [UIView animateWithDuration:0.7 animations:^{
        self.frontView.frame = CGRectMake(0, 175, 320, self.frontView.frame.size.height);
        
        self.blurView.frame = CGRectMake(0, 175, 320, self.blurView.frame.size.height);
    }];
}

- (void)slideDownWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer {
    
    [UIView animateWithDuration:0.7 animations:^{
        self.frontView.frame =CGRectMake(0, 480, 320, self.frontView.frame.size.height);
        
        self.blurView.frame =CGRectMake(0, 480, 320, self.blurView.frame.size.height);
    }];
}

- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer {
    
    CGFloat newYCoordinate = 175.0;
    
    if(self.frontView.frame.origin.y == 175.0) {
        newYCoordinate = 480.0;
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        self.frontView.frame = CGRectMake(self.frontView.frame.origin.x, newYCoordinate, self.frontView.frame.size.width, self.frontView.frame.size.height);
        
        self.blurView.frame = CGRectMake(self.blurView.frame.origin.x, newYCoordinate, self.blurView.frame.size.width, self.blurView.frame.size.height);
    }];
}

- (IBAction)onClickChat:(id)sender {
    
    [self performSegueWithIdentifier:@"segueToDirectChat" sender:self];
}

- (IBAction)onClickPlaceIcon:(id)sender {
    [self.viewForPlace setHidden:NO];
    [self.viewForDoc setHidden:YES];
    [self.viewForPerson setHidden:YES];
    
    [self.btnPlaceIcon setImage:[UIImage imageNamed:@"icon_pin_green.png"] forState:UIControlStateNormal];
    [self.btnDocIcon setImage:[UIImage imageNamed:@"icon_doc_gray.png"] forState:UIControlStateNormal];
    [self.btnPersonIcon setImage:[UIImage imageNamed:@"icon_person_gray.png"] forState:UIControlStateNormal];
}

- (IBAction)onClickDocIcon:(id)sender {
    [self.viewForPlace setHidden:YES];
    [self.viewForDoc setHidden:NO];
    [self.viewForPerson setHidden:YES];
    
    [self.btnPlaceIcon setImage:[UIImage imageNamed:@"icon_pin_gray.png"] forState:UIControlStateNormal];
    [self.btnDocIcon setImage:[UIImage imageNamed:@"icon_doc_green.png"] forState:UIControlStateNormal];
    [self.btnPersonIcon setImage:[UIImage imageNamed:@"icon_person_gray.png"] forState:UIControlStateNormal];
}

- (IBAction)onClickPersonIcon:(id)sender {
    [self.viewForPlace setHidden:YES];
    [self.viewForDoc setHidden:YES];
    [self.viewForPerson setHidden:NO];
    
    [self.btnPlaceIcon setImage:[UIImage imageNamed:@"icon_pin_gray.png"] forState:UIControlStateNormal];
    [self.btnDocIcon setImage:[UIImage imageNamed:@"icon_doc_gray.png"] forState:UIControlStateNormal];
    [self.btnPersonIcon setImage:[UIImage imageNamed:@"icon_person_green.png"] forState:UIControlStateNormal];
}

@end
