//
//  RideNextDriverCell.m
//  SwiftBack
//
//  Created by My Mac on 10/16/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "ConfirmOfferCell.h"

@implementation ConfirmOfferCell

- (void)awakeFromNib {
	[self.ratingView initRateBar];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnEnlargeImg:(id)sender {
    
    
 //   [_driverImgView.layer setCornerRadius:_driverImgView.frame.size.width / 2];
  //  [_driverImgView setBackgroundColor:[UIColor redColor]];
    
    [UIView animateWithDuration:1 animations:^{
        
        // Animate it to double the size
        const CGFloat scale = 1;
      //  [_driverImgView setTransform:CGAffineTransformMakeScale(scale, scale)];
        
       // _driverImgView.frame =CGRectMake(70,15,150,150);
    }];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    [self.overlayDriver.layer addAnimation:transition forKey:nil];
   // [self.lblDriverName2.layer addAnimation:transition forKey:nil];
    
    _overlayDriver.hidden = NO;
    _overlayButton.hidden = NO;
    //_lblDriverName2.hidden = NO;
}

UIView *overlayDriver;

-(void)addOverlay{
    //Add the overlay, if there's one in your code, then you don't have to create this
    overlayDriver = [[UIView alloc] initWithFrame:CGRectMake(0,  0,self.overlayDriver.frame.size.width, self.overlayDriver.frame.size.height)];
    [overlayDriver setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    
    //Register the tap gesture recognizer
    UITapGestureRecognizer *overlayTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(onOverlayDriverTapped)];
    
    [overlayDriver addGestureRecognizer:overlayTap];
    
    [self.overlayDriver addSubview:overlayDriver];
}


- (void)onOverlayDriverTapped
{
    //Call your dismiss method
    
    for (UITapGestureRecognizer *ges in overlayDriver.gestureRecognizers) {
        [overlayDriver removeGestureRecognizer:ges];
        _overlayDriver.hidden = YES;
        
    }
    [overlayDriver removeFromSuperview];
    
}
- (IBAction)overlayButton:(id)sender {
    
   // [_driverImgView.layer setCornerRadius:40];
  //  [_driverImgView setBackgroundColor:[UIColor redColor]];
    //[_driverImgView.layer applyRoundedCornersFullWithColor:[UIColor colorWithRed:0.30 green:0.71 blue:0.67 alpha:1.0]];
    
    [UIView animateWithDuration:1 animations:^{
        
        // Animate it to double the size
        const CGFloat scale = 1;
    //    [_driverImgView setTransform:CGAffineTransformMakeScale(scale, scale)];
        
    //    _driverImgView.frame =CGRectMake(0,130,80,80);
    }];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    [self.overlayDriver.layer addAnimation:transition forKey:nil];
  //  [self.lblDriverName2.layer addAnimation:transition forKey:nil];
    _overlayDriver.hidden = YES;
    _overlayButton.hidden = YES;
 //   _lblDriverName2.hidden = YES;
    
    
    
}
     

@end
