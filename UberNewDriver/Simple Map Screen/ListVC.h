
#import "BaseVC.h"
#import "RatingBar.h"

@protocol dataParser <NSObject>

-(void)listLoaded:(NSDictionary *)dictData;

@end

@interface ListVC : BaseVC<UITableViewDelegate,UITableViewDataSource>
{
    BOOL internet;
}
@property(strong,nonatomic) id<dataParser> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imgNoItems;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

@property (weak, nonatomic) IBOutlet UITableView *tblRequests;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;


- (void)onClickAcceptRide:(UIButton*)sender;
- (void)onClickRejectRide:(UIButton*)sender;

@property(nonatomic, strong) NSTimer *timerGetRequests;

- (IBAction)backButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *mainView;

@end
