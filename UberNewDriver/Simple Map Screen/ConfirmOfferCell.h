//
//  RideNextDriverCell.h
//  SwiftBack
//
//  Created by My Mac on 10/16/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "RatingBar.h"


@interface ConfirmOfferCell : SWTableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblFromStation;
@property (weak, nonatomic) IBOutlet UILabel *lblToStation;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundDriver;
@property (weak, nonatomic) IBOutlet UIImageView *overlayDriver;
@property (weak, nonatomic) IBOutlet UIButton *overlayButton;
@property (weak, nonatomic) IBOutlet UIImageView *passengerImgView;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@property (weak, nonatomic) IBOutlet UIButton *markBtn;

@end
