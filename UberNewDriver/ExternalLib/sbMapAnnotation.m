

#import "SBMapAnnotation.h"

@implementation SBMapAnnotation

@synthesize coordinate,title,subtitle,yTag,pinColor;
- (id)initWithCoordinate:(CLLocationCoordinate2D) inCoordinate {
    coordinate = inCoordinate;
    return self;
}

@end