

#import <UIKit/UIKit.h>

@interface UIView (Utils)

- (void)applyRoundedCorners;
- (void)applyRoundedCornersLess;
- (void)applyRoundedCornersFull;
- (void)applyRoundedCornersFullWithColor:(UIColor *)color;

@end
