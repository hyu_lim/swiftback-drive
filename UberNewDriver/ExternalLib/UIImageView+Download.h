
#import <UIKit/UIKit.h>

@interface UIImageView (Download)

-(void)downloadFromURL:(NSString *)url withPlaceholder:(UIImage *)placehold;
-(void)downloadFromURL:(NSString *)url;
@end
