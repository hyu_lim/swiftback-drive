
#import "ViewController.h"
#import "LoginVC.h"
#import <CoreLocation/CoreLocation.h>
#import "Constant.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface ViewController ()
{
    CLLocationManager *locationManager;
    
    BOOL internet;
    BOOL IS_LOGIN;
    NSMutableDictionary *dictparam;
    
    NSMutableString * strEmail;
    NSMutableString * strPassword;
    NSString * strLogin;
    NSString * strSocialId;
    
}
@property (nonatomic, strong) AVPlayer *avplayer;
@end

@implementation ViewController


#pragma mark -
#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    
    [self setLocalization];
    
    internet=[APPDELEGATE connected];
    if ([CLLocationManager locationServicesEnabled])
    {
        if([APPDELEGATE connected])
        {
            [self getUserLocation];
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLEt", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"ENABLE_LOCATION_ACCESS", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
    
    // Do any additional setup after loading the view, typically from a nib.
    
    [super viewDidLoad];
    
    /*[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(move_pagination) userInfo:nil repeats:YES]; */
    
    
    
    // Create the data model
    
    
    _pageTitles = @[NSLocalizedString(@"DRIVER", nil), NSLocalizedString(@"SHARE_YOUR_RIDE", nil), NSLocalizedString(@"EARN_BY_SHARING", nil), NSLocalizedString(@"MEET_NEW_PEOPLE", nil), NSLocalizedString(@"HELP_SAVE_THE_ENVIRONMENT", nil)];
    
    _pageImages = @[@" ", @"icon_car.png", @"icon_2x.png", @"icon_people.png", @"icon_leaf.png"];
    
    _pageTitles2 = @[@"SwiftBack", @" ", @" ", @" ", @" "];
    
    _pageImages2 = @[@"arrow_logo.png", @" ", @" ", @" ", @" "];
    
    _pageTitles3 = @[NSLocalizedString(@"", nil), NSLocalizedString(@"CARPOOL_PEOPLE_ALONG_YOUR_WAY", nil), NSLocalizedString(@"SAVE_A_FEW_HUNDREDS_A_MONTH", nil), NSLocalizedString(@"ENJOY_A_NEW_SOCIAL_EXPERIENCE", nil), NSLocalizedString(@"BY_CUTTING_DOWN_ON_CARBON_FOOTPRINT", nil)];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroPageViewController"];
    self.pageViewController.dataSource = self;
    
    IntroPageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 50);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    //page controller properties
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.5];
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    pageControl.backgroundColor = [UIColor clearColor];
    
    /*_backgroundRect.layer.cornerRadius = 10;
     _backgroundRect.layer.borderWidth = 1;
     _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
     _backgroundRect.layer.shadowRadius = 5.0;
     _backgroundRect.layer.shadowOpacity = 0.4;
     */
    
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"SB_LANDING" ofType:@"mp4"]];
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x156560) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x156560) CGColor],nil];
    [self.gradintView.layer insertSublayer:gradient atIndex:0];
    
    
    // [self.btnSignin setTitle:NSLocalizedString(@"SIGN IN", nil) forState:UIControlStateNormal];
    /// [self.btnRegister setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateNormal];
}


- (void)setLocalization {
    [self.btnRegister setTitle:NSLocalizedString(@"DRIVER_", nil) forState:UIControlStateNormal];
    [self.btnPassenger setTitle:NSLocalizedString(@"PASSENGER_", nil) forState:UIControlStateNormal];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    dictparam=[[NSMutableDictionary alloc]init];
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    IS_LOGIN=[pref boolForKey:PREF_IS_LOGIN];
    
    if(IS_LOGIN)
    {
        strEmail=[pref objectForKey:PREF_EMAIL];
        strPassword=[pref objectForKey:PREF_PASSWORD];
        strLogin=[pref objectForKey:PREF_LOGIN_BY];
        strSocialId=[pref objectForKey:PREF_SOCIAL_ID];
        device_token=[pref objectForKey:PREF_DEVICE_TOKEN];
        if (strEmail)
        {
        
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"close"])
            {
                NSDate *closeDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"closeDate"];
                NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:closeDate];
                
                if (secondsBetween <= 280 && [[[NSUserDefaults standardUserDefaults] objectForKey:PARAM_OTP_VERIFY] integerValue]== 0)
                {
                    [self performSegueWithIdentifier:SEGUE_TO_OTP sender:nil];
                }
                else
                {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PARAM_OTP_VERIFY] integerValue]== 1)
                    {
                        [self getSignIn];
                    }
                }
            }
            else
            {
                //if ([[[NSUserDefaults standardUserDefaults] objectForKey:PARAM_OTP_VERIFY] integerValue]== 1)
               /// {
                    [self getSignIn];
                //}
               
            }
        }
    }
    else
    {
        self.navigationController.navigationBarHidden=YES;
    }
    
    self.navigationController.navigationBarHidden=YES;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.avplayer play];
}

-(void)viewDidDisappear:(BOOL)animated
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    IS_LOGIN=[pref boolForKey:PREF_IS_LOGIN];
    
//    if(!IS_LOGIN)
//        [self.navigationController setNavigationBarHidden:YES animated:NO];
}

//AV PLAYER

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}

//AV PLAYER END

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //segue.identifier
}

- (IBAction)btnPassenger:(id)sender {
    
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"SWIFTBACK_PASSENGER", nil)
                                                       message:NSLocalizedString(@"DOWNLOAD_PASSENGER_APP", nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"NO", nil)
                                             otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
        [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            
        }
    }
    
    // 0 = Tapped yes
    if (buttonIndex == 1)
    {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://swiftback.com/passengerapp/"]];
        
    }
}

#pragma mark -
#pragma mark - Sign In

-(void)getSignIn
{
    if (strEmail==nil)
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strEmail=[pref objectForKey:PREF_EMAIL];
        strPassword=[pref objectForKey:PREF_PASSWORD];
        strLogin=[pref objectForKey:PREF_LOGIN_BY];
        strSocialId=[pref objectForKey:PREF_SOCIAL_ID];
        if (strLogin == nil || [strLogin length] < 1) {
            strLogin = @"manual";
            strSocialId = @"";
        }
    }
    
    if ( [strEmail length] > 1) {
        if([APPDELEGATE connected] )
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"SIGN_IN", nil)];
            
            if ([device_token  length] < 1 || [device_token isKindOfClass:[NSNull class]]) {
                device_token = @"1212121212";
            }
            
            [dictparam setObject:device_token forKey:PARAM_DEVICE_TOKEN];
            [dictparam setObject:@"ios" forKey:PARAM_DEVICE_TYPE];
            [dictparam setObject:strEmail forKey:PARAM_EMAIL];
            
            NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
            NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
            
            
            [dictparam setObject:majorVersion forKey:PARAM_VERSION];
            
            [dictparam setObject:strLogin forKey:PARAM_LOGIN_BY];
            if (![strLogin isEqualToString:@"manual"])
            {
                [dictparam setObject:strSocialId forKey:PARAM_SOCIAL_ID];
                
            }
            else
            {
                [dictparam setObject:strPassword forKey:PARAM_PASSWORD];
            }
            
            
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_LOGIN withParamData:dictparam withBlock:^(id response, NSError *error)
             {
                 [APPDELEGATE hideLoadingView];

                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         arrUser=response;
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setObject:[response valueForKey:@"token"] forKey:PREF_USER_TOKEN];
                         [pref setObject:[response valueForKey:@"id"] forKey:PREF_USER_ID];
                         [pref setObject:[response valueForKey:@"is_approved"] forKey:PREF_IS_APPROVED];
                         [pref synchronize];
                         
                         
                         NSString *strLog=[NSString stringWithFormat:@"%@ %@ 😛",NSLocalizedString(@"LOGIN_SUCCESS", nil),[response valueForKey:@"first_name"]];
                         
                         [APPDELEGATE showToastMessage:strLog];
                         [self performSegueWithIdentifier:@"segueToDirectLogin" sender:self];
                         //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"REGISTER_SUCCESS", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         //            [alert show];
                     }
                     else
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"SIGNIN_FAILED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
                 
                 [APPDELEGATE hideLoadingView];
                 NSLog(@"REGISTER RESPONSE --> %@",response);
             }];
        }
        else
        {
            [APPDELEGATE hideLoadingView];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }

    }
    }

#pragma mark-
#pragma mark- Get Location

-(void)getUserLocation
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    
    [locationManager startUpdatingLocation];
    
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        struser_lati=[NSString stringWithFormat:@"%.8f",currentLocation.coordinate.latitude];//[NSString stringWithFormat:@"%.8f",22.30];//
        struser_longi=[NSString stringWithFormat:@"%.8f",currentLocation.coordinate.longitude];//[NSString stringWithFormat:@"%.8f",70.78];//
    }
    
    
    // stop updating location in order to save battery power
    [locationManager stopUpdatingLocation];
    
    
    // Reverse Geocoding
    // NSLog(@"Resolving the Address");
    
    // “reverseGeocodeLocation” method to translate the locate data into a human-readable address.
    
    // The reason for using "completionHandler" ----
    //  Instead of using delegate to provide feedback, the CLGeocoder uses “block” to deal with the response. By using block, you do not need to write a separate method. Just provide the code inline to execute after the geocoding call completes.
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         // NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         if (error == nil && [placemarks count] > 0)
         {
             CLPlacemark *placemark = [placemarks lastObject];
             
             // strAdd -> take bydefault value nil
             NSString *strAdd = nil;
             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             
             if ([placemark.postalCode length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
                 else
                     strAdd = placemark.postalCode;
             }
             
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
             }
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                 else
                     strAdd = placemark.country;
                 
             }
             
         }
     }];
}

#pragma mark-
#pragma mark- Alert Button Clicked Event




- (IBAction)onClickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}
//

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((IntroPageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((IntroPageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (IntroPageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    IntroPageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroPageContentController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.imageFile2 = self.pageImages2[index];
    pageContentViewController.titleText2 = self.pageTitles2[index];
    pageContentViewController.titleText3 = self.pageTitles3[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

@end
