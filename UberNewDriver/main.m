//
//  main.m
//  UberNewDriver
//
//  Developed by Elluminati on 27/09/14.
//  Copyright (c) 2015 Swiftback. All rights reserved.
// 

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
		
    }
}
