//
//  ChatVC.h
//  SwiftBack
//
//  Created by Elluminati on 04/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSMutableArray *arrMessages;
    NSString *passengerEmail;
    
    float tableHeight,viewPosition;

    QBUUser *receipent;
    
    __weak IBOutlet UITextField *txtMessage;
    __weak IBOutlet UITableView *tableForChat;
    __weak IBOutlet UIButton *menuBtn;
    __weak IBOutlet UIView *viewForSender;
}

@property (nonatomic, strong) QBUUser *receipent;
@property (nonatomic, strong) QBChatDialog *receipentDialog;
@property  NSInteger type;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblNameAtNavBar;
@property (weak, nonatomic) IBOutlet UIImageView *lblProfileImage;


- (IBAction)onClickSend:(id)sender;
- (IBAction)onClickBack:(id)sender;

@end
