//
//  DialogsVC.m
//  SwiftBack
//
//  Created by Elluminati on 04/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "DialogsVC.h"
#import "ServicesManager.h"
#import "ChatVC.h"
#import "SWRevealViewController.h"


@interface DialogsVC ()

@end

@implementation DialogsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self loginForQuickBlox];
    
    [APPDELEGATE showLoadingWithTitle:@"Loading"];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkForLogin) userInfo:nil repeats:YES];

    [self customSetup];
    if (!isQuickbloxConnected) {
        [self loginForQuickBlox];
        
    }    //[self loadDialogs];
    // Do any additional setup after loading the view.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}


-(void)checkForLogin
{
    if (isQuickbloxConnected) {
        //[APPDELEGATE hideLoadingView];

        [self loadDialogs];
        [timer invalidate];
        timer = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loginForQuickBlox{
    
   // [APPDELEGATE showLoadingWithTitle:@"Loading"];
    NSDictionary *dictLogin = [USERDEFAULT objectForKey:PREF_LOGIN_OBJECT];
    
    NSString *fullname = [NSString stringWithFormat:@"%@ %@",[dictLogin objectForKey:PARAM_FIRST_NAME],[dictLogin objectForKey:PARAM_LAST_NAME]];
    
    
    QBUUser *user = [QBUUser user];
    [user setLogin:fullname];
    fullname = [fullname stringByAppendingFormat:@"%@",[dictLogin objectForKey:PARAM_ID]];
    [user setPassword:fullname];
    
    
    
    [[QMServicesManager instance] logInWithUser:user completion:^(BOOL success, NSString *errorMessage) {
        if (success) {
            
            currentUser = user;
            [USERDEFAULT setObject:user forKey:@"QBUSER"];
            [USERDEFAULT synchronize];
            [[QBChat instance] connectWithUser:user completion:^(NSError * _Nullable error) {
                
                NSString *stringCheck = [error.userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
                if ([stringCheck isEqualToString:@"You are already connected to chat."] || !error) {
                    
                    [APPDELEGATE showLoadingWithTitle:@"Loading"];

                    [self loadDialogs];
                }
                else{
                }
            }];
            
        }
        else{
        }
    }];
}

- (void)loadDialogs
{
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:@"Loading"];

   // __weak __typeof(self) weakSelf = self;
    __block   NSMutableArray *tempObjects = [[NSMutableArray alloc]init];
    [[ServicesManager instance].chatService allDialogsWithPageLimit:kDialogsPageLimit extendedRequest:nil iterationBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, BOOL *stop) {
       // __typeof(weakSelf) strongSelf = weakSelf;
         [tempObjects  addObjectsFromArray:dialogObjects];
    } completion:^(QBResponse *response)
     {
         if ([tempObjects count] > 0) {
             
             if ([ServicesManager instance].isAuthorized) {
                 if (response.success) {
                     //  [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"SA_STR_COMPLETED", nil)];
                     //  [ServicesManager instance].lastActivityDate = [NSDate date];
                     arrQBDialogs = tempObjects;
                     [tableForDialogs setHidden:NO];
                     [tableForDialogs reloadData];
                     [APPDELEGATE hideLoadingView];

                     //[self.navigationController setNavigationBarHidden:NO];
                 }
                 else {
                     // [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"SA_STR_FAILED_LOAD_DIALOGS", nil)];
                 }
             }
         }
         [APPDELEGATE hideLoadingView];
         
     }];
    //}
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrQBDialogs count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }

    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.detailTextLabel.textColor = [UIColor darkGrayColor];

    
    [cell.textLabel setText:[[arrQBDialogs objectAtIndex:indexPath.row] name]];
    [cell.detailTextLabel setText:[[arrQBDialogs objectAtIndex:indexPath.row] lastMessageText]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    QBChatDialog *dialog = [arrQBDialogs objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"segueToChatVC" sender:dialog];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"segueToChatVC"]) {
        ChatVC *chat = [segue destinationViewController];
        [chat setType:0];
        [chat setReceipentDialog:sender];
    }
  
}

- (IBAction)onClickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
