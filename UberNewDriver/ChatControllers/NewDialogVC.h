//
//  NewDialogVC.h
//  SwiftBack
//
//  Created by Elluminati on 04/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewDialogVC : UIViewController<UITableViewDelegate,UITableViewDataSource>{

    NSMutableArray *arrQBWalkers;
    __weak IBOutlet UITableView *tableForNewDialogs;
    
    __weak IBOutlet UIButton *menuBtn;
}
- (IBAction)onClickBack:(id)sender;

@end
