

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController
{
    BOOL animPop;
}
-(void)setBackBarItem;
-(void)setBackBarItem:(BOOL)animated;
-(void)setNavBarTitle:(NSString *)title;

@end
