

#import "TutorialViewController.h"
#import "ViewController.h"


@interface TutorialViewController ()

@end

@implementation TutorialViewController

- (void)viewDidLoad
{
    [self setNeedsStatusBarAppearanceUpdate];
    
    [super viewDidLoad];
    
    [self setLocalization];
    
    // Create the data model
    _pageTitles = @[NSLocalizedString(@"REGISTER_FOR_FREE", nil), NSLocalizedString(@"GET_RIDE_REQUESTS", nil), NSLocalizedString(@"GET_REWARDED", nil), NSLocalizedString(@"", nil)];
    
        _pageTitles2 = @[NSLocalizedString(@"GET_VERIFIED_AND_GET_PAID", nil), NSLocalizedString(@"FROM_PEOPLE_AROUND", nil), NSLocalizedString(@"&_CUT_YOUR_COST", nil), NSLocalizedString(@"", nil)];
    
        _pageImages = @[@"test_tut2.png", @"test_tut3.png", @"test_tut4.png", @"test_tut5.png"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    [self.view bringSubviewToFront:_btnBack];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(HandleNewRequestPush:)
                                                 name:@"New Request" object:nil];
}

- (void)setLocalization {
    [self.BtnLogin setTitle:NSLocalizedString(@"LOG_IN_", nil) forState:UIControlStateNormal];
    [self.BtnSignUp setTitle:NSLocalizedString(@"SIGN_UP_", nil) forState:UIControlStateNormal];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
/*
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

 

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}
*/


//update status bar
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)HandleNewRequestPush:(NSNotification*)userInfo
{
    [self performSegueWithIdentifier:@"segueToHome" sender:self];
    
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToDriver:(id)sender {
    // A quick and dirty popup, displayed only once
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"HasSeenPopup"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"SWIFTBACK_TITLE", nil)
                                                       message:NSLocalizedString(@"DOWNLOAD_PASSENGER_APP", nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"NO", nil)
                                             otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
        [alert show];
    }
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // 0 = Tapped yes
    if (buttonIndex == 1)
    {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.swiftback.com/passengerapp"]];
    }
}

- (IBAction)startWalkthrough:(id)sender {
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *TutorialViewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:TutorialViewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    if (([self.pageTitles2 count] == 0) || (index >= [self.pageTitles2 count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.titleText2 = self.pageTitles2[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

//Back Button

- (IBAction)btnBackClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
    
    if (index == [self.pageTitles2 count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
    return [self.pageTitles2 count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (IBAction)onClickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
