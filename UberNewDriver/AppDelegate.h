

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#define Google_Key @"AIzaSyBlJu2UTwemZ8a9uWCGLOiG_OAdN9alUhQ"
#define Google_Browser_key @"AIzaSyA0iwWpcCS13NTIDqsCR6MeHqT7z0nRQQM"

#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIView   *viewLoading;

+(AppDelegate *)sharedAppDelegate;

- (void)userLoggedIn;
- (NSString *)applicationCacheDirectoryString;
- (void) showHUDLoadingView:(NSString *)strTitle;
- (void) hideHUDLoadingView;
- (void)showToastMessage:(NSString *)message;
- (void)showLoadingWithTitle:(NSString *)title;
- (void)hideLoadingView;
- (BOOL)connected;
- (id)setBoldFontDiscriptor:(id)objc;

@end
