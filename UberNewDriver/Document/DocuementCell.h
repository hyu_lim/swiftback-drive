//
//  DocuementCell.h
//  SwiftBack Driver
//
//  Created by Rohan on 27/01/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocuementCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UILabel *lblSubType;
@property (weak, nonatomic) IBOutlet UIButton *docBtn;

@end
