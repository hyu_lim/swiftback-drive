//
//  BankAccountVc.h
//  SwiftBack Driver
//
//  Created by Rohan on 28/01/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankAccountVC : UIViewController<UITextFieldDelegate>
{
    
    __weak IBOutlet UITextField *txtAccountType;
    
    __weak IBOutlet UITextField *txtAccountNumber;
    
    __weak IBOutlet UITextField *txtAddress;
    
    __weak IBOutlet UITextField *txtRoutingNumber;
    
    __weak IBOutlet UIScrollView *scrollView;
    
}

@property (weak, nonatomic) IBOutlet UIView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

- (IBAction)onClickSubmit:(id)sender;
- (IBAction)onClickSkip:(id)sender;

@end
