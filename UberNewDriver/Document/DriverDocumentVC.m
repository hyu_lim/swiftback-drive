//
//  DriverDocumentVC.m
//  SwiftBack Driver
//
//  Created by Rohan on 27/01/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "DriverDocumentVC.h"
#import "DocuementCell.h"

@interface DriverDocumentVC ()

@end

@implementation DriverDocumentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getDocumentTypes];
    
    //edit sub-type here
    arrDocumentSubTypes = [[NSMutableArray alloc] initWithObjects:@"SubType1",@"SubType2",@"SubType3", nil];
    
   
    [self getDocumentTypes];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults boolForKey:@"isFromBank"]) {
        [defaults setBool:NO forKey:@"isFromBank"];
        [defaults synchronize];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)getDocumentTypes
{
    if ([APPDELEGATE connected]) {
        
        [APPDELEGATE showLoadingWithTitle:@"Loading..."];
        
        NSUserDefaults *defaults = [[NSUserDefaults alloc]init];
        
        NSMutableDictionary *dictParam  = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[defaults objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[defaults objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_DOCUMENT_LIST withParamData:dictParam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response)
            {
                NSLog(@"%@",response);
                NSArray *rawData = [response valueForKey:@"documents"] ;
                arrDocumentTypes=[[[NSMutableArray alloc]initWithArray:rawData] mutableCopy];
                
                for (int d =0; d <[arrDocumentTypes count]; d++) {
                    
                    NSMutableDictionary *dictProof = [[arrDocumentTypes objectAtIndex:d] mutableCopy];
                    [dictProof setObject:@"" forKey:PARAM_IMAGE];
                    
                    [arrDocumentTypes replaceObjectAtIndex:d withObject:dictProof];
                }
                
                [tableForDocument reloadData];
            }
        }];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrDocumentTypes count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DocuementCell *Cell = [tableForDocument dequeueReusableCellWithIdentifier:@"cell"];
    
    [Cell.lblType setText:[NSString stringWithFormat:@"%@.",[[arrDocumentTypes objectAtIndex:indexPath.row] objectForKey:PARAM_NAME]]];
    
    [Cell.lblSubType setText:[arrDocumentSubTypes objectAtIndex:indexPath.row]];
    
    [Cell.lblType setTextColor:[UIColor darkGrayColor]];
//    [Cell.lblType setFont:[UberStyleGuide fontRegular:16.0]];
    
    if ([[[arrDocumentTypes objectAtIndex:indexPath.row] objectForKey:PARAM_IMAGE]isKindOfClass:[UIImage class]])
    {
        [Cell.docBtn setBackgroundImage:[[arrDocumentTypes objectAtIndex:indexPath.row] objectForKey:PARAM_IMAGE]forState:UIControlStateNormal];
    }
    
    [Cell.docBtn setTag:indexPath.row];
    [Cell.docBtn addTarget:self action:@selector( onClickDocumentImage:) forControlEvents:UIControlEventTouchUpInside];
    [Cell.docBtn.layer setCornerRadius:30.0];
    [Cell.docBtn setClipsToBounds:YES];
    
    return Cell;
}

-(IBAction)onClickDocumentImage:(UIButton*)sender
{
    
    actionProof = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Gallery", @"Camera", nil];
    [actionProof setTag:sender.tag];
    [actionProof showInView:self.view];
}

#pragma mark
#pragma mark - ActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            [self selectPhotos];
        }
            break;
        case 1:
        {
            [self takePhoto];
        }
            break;
        default:
            break;
    }
}
#pragma mark
#pragma mark - Action to Share
- (void)selectPhotos
{
    // Set up the image picker controller and add it to the view
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.allowsEditing=YES;
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}
-(void)takePhoto
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType =UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing=YES;
        [self presentViewController:imagePickerController animated:YES completion:^{
        }];
    }
    else
    {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"CAM_NOT_AVAILABLE", nil)delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alt show];
    }  // Set up the image picker controller and add it to the view
}


#pragma mark
#pragma mark - ImagePickerDelegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if([info valueForKey:UIImagePickerControllerEditedImage]==nil)
    {
        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
        [assetLibrary assetForURL:[info valueForKey:UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset) {
            ALAssetRepresentation *rep = [asset defaultRepresentation];
            Byte *buffer = (Byte*)malloc((long)rep.size);
            NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:(long)rep.size error:nil];
            NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];//
            UIImage *img=[UIImage imageWithData:data];
            [self setImage:img];
        } failureBlock:^(NSError *err) {
            NSLog(@"Error: %@",[err localizedDescription]);
        }];
    }
    else
    {
        [self setImage:[info valueForKey:UIImagePickerControllerEditedImage]];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)setImage:(UIImage *)image
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:[arrDocumentTypes objectAtIndex:actionProof.tag]];
    [dict setObject:image forKey:PARAM_IMAGE];
    [arrDocumentTypes replaceObjectAtIndex:actionProof.tag withObject:dict];
    [tableForDocument reloadData];
    isAdded = YES;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)onClickNext:(id)sender {
    
    if ([APPDELEGATE connected]) {
        
        if (isAdded) {
            
            [APPDELEGATE showLoadingWithTitle:@"Loading"];
            NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
            
            //Note:- do not put image inside parameters dictionary as I did, but append it!
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [dictParam setObject:[defaults objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setObject:[defaults objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            
            AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [helper getDataFromPath:FILE_DRIVER_DOCUMENT withParamDataImages:dictParam andImages:arrDocumentTypes withBlock:^(id response, NSError *error) {
                
                [APPDELEGATE hideLoadingView];
                if (response) {
                    if ([[response valueForKey:@"success"] boolValue]) {
                        
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Documents uploaded successfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alert show];
                        [self performSegueWithIdentifier:@"goToBank" sender:nil];
                    }
                    else{
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Internal server eror, Please try again", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
                else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Internal server eror, Please try again", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
            
        }
        
    } else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)onClickSkip:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}
@end
