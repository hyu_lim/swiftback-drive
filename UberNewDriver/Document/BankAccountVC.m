//
//  BankAccountVc.m
//  SwiftBack Driver
//
//  Created by Rohan on 28/01/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "BankAccountVC.h"

@interface BankAccountVC ()

@end

@implementation BankAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [scrollView setContentSize:CGSizeMake(0, 200)];
    
    // backgroundRect
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor colorWithRed:50/255.0
                                                        green:169/255.0
                                                         blue:142/255.0
                                                        alpha:1].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    // btnSkip
    _btnSkip.layer.cornerRadius = 5;
    _btnSkip.layer.borderWidth = 0.7;
    _btnSkip.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // btnSubmit
    _btnSubmit.layer.cornerRadius = 5;
    _btnSubmit.layer.borderWidth = 0.7;
    _btnSubmit.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    

    if (!IS_IPHONE5) {
        if (textField == txtAccountType) {
            [scrollView setContentOffset:CGPointMake(0, 30) animated:YES];
        }
        else if (textField == txtAccountNumber){
            [scrollView setContentOffset:CGPointMake(0, 60) animated:YES];
        }
        else if (textField == txtAddress){
            [scrollView setContentOffset:CGPointMake(0, 90) animated:YES];
        }
        else if (textField == txtRoutingNumber){
            [scrollView setContentOffset:CGPointMake(0, 120) animated:YES];
        }
        else{
            [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == txtAccountType)
        [txtAccountNumber becomeFirstResponder];
    else if (textField == txtAccountNumber)
        [txtAddress becomeFirstResponder];
    else if (textField == txtAddress)
        [txtRoutingNumber becomeFirstResponder];
    else
        [textField resignFirstResponder];
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];

}



- (IBAction)onClickSubmit:(id)sender {
    
    if ([APPDELEGATE connected]) {
        
        if ([[[UtilityClass sharedObject] trimString:txtAccountType.text] length]  < 1) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please enter account type", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([[[UtilityClass sharedObject] trimString:txtAccountNumber.text] length] < 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLease enter account number", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([[[UtilityClass sharedObject] trimString:txtAddress.text] length] < 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please enter address", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
            [dictParam setObject:[defaults objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setObject:[defaults objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            
            [dictParam setObject:txtAccountType.text forKey:PARAM_ACCOUNT_TYPE];
            [dictParam setObject:txtAccountNumber.text forKey:PARAM_ACCOUNT_NUMBER];
            [dictParam setObject:txtAddress.text forKey:PARAM_ADDRESS];
            [dictParam setObject:txtRoutingNumber.text forKey:PARAM_ROUTING_NUMBER];
            
            [APPDELEGATE showLoadingWithTitle:@"Loading..."];
            AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [helper getDataFromPath:FILE_BANKDETAILS withParamData:dictParam withBlock:^(id response, NSError *error) {
                [APPDELEGATE hideLoadingView];
                if (response) {
                    if ([[response valueForKey:@"success"]boolValue]) {
                        
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Documents Sucessfully Submitted" message:@"Pending admin approval in 24 - 48 hrs" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                        [alert show];
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setBool:YES forKey:@"isFromBank"];
                        [defaults synchronize];
                        [self dismissViewControllerAnimated:YES completion:nil];
                        
                        

                    }
                    else{
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Internal server eror, Please try again", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }];
        }
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)onClickSkip:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
